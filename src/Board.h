#pragma once

#include "Position.h"
#include "Piece.h"

namespace chess
{
	enum Status { Normal, InCheck, Stalemate, Checkmate, Dead };

	bool findIn(const PosList& posList, const Position& pos );

	struct CmdInfo {
		int mFrame;

		CmdInfo(int frame) : mFrame(frame) {}
		virtual bool execute(Board& board) = 0;
		virtual void undo(Board& board) = 0;
	};

	struct AddOverCmdInfo;

	struct SlideInfo {
		SlideInfo(const Maneuver& move, Piece* piece, int start, int end) : mMove(move), mPiece(piece), mStart(start), mEnd(end) {}
		Maneuver mMove;
		Piece* mPiece;
		int mStart;
		int mEnd;
	};

	struct LightInfo
	{
		LightInfo(const Position& pos, const ofColor& color, int frames) : mFrames(frames), mColor(color), mPos(pos), mStartPos(pos) {};
		LightInfo(const Position& pos, const ofColor& color, int frames, const Position& startPos, int startFrame) : mFrames(frames), mColor(color), mPos(pos), mStartPos(startPos), mStartFrame(startFrame) {};

		int mFrames;
		int mStartFrame;
		ofColor mColor;
		Position mPos;
		Position mStartPos;
	};

	class Board
	{
	public:
		//////////////////////////////////////////////////////////////////////////
		// Publics
		//////////////////////////////////////////////////////////////////////////

		Board();
		virtual ~Board();
		void setup(int players, int height, int width, const string& layout);

		bool hasPosition(const Position& pos) const;
		Piece* at(const Position& pos);
		void advance();
		int evaluateBoard(int color);
		void undoFrame();
		void updateRenderer();
		void draw();
		void calc();
		void print() const;
		void reset();

		bool inBounds(const Position& pos) const;
		bool inside(const ofVec2f& pos) const; // screen position inside board

		bool getLayout(const Position& pos) const;
		void setLayout(const Position& pos, bool valid);
		string getFullLayout() { return mLayout; }

		void getMoves(int color, MoveList& moves) const;
		bool isVulnerable(int color, const Position& pos) const;
		bool isValidMove(int color, const Maneuver& move);
		Status getPlayerStatus(int color);
		int getPlayerCount() { return mPlayerPieces.size(); }

		bool execute(shared_ptr<CmdInfo> cmd);
		void queueCmd(shared_ptr<CmdInfo> cmd) { mQueueCmds.push_back(cmd); }
		void keepCmd(shared_ptr<AddOverCmdInfo> cmd) { mKeepCmds.push_back(cmd); }  // don't delete these 
		void removeKeepCmd(shared_ptr<AddOverCmdInfo> cmd) { mKeepCmds.remove(cmd); }  // don't delete these 
		void undo();

		bool cmdAdd(char pieceName, int color, const Position& pos);
		bool cmdMove(const Maneuver& move);
		bool cmdSlide(const Maneuver& move);

		void lightUp(const PosList& poslist, int frames, const ofColor& color);
		void slightUp(const PosList& poslist, const Position& startPos, int frames, int startFrame, const ofColor& color);
		void lightUpValid(Piece* piece);
		void lightUpAttack(Piece* piece);
		void lightUp(const Position& pos, const ofColor& color, int frames) {
			mLightUps.push_back(LightInfo(pos, color, frames));
		}

		int getFrame() { return mFrame; }
		int getWidth() const;
		int getHeight() const;
		Position modPos(Position pos) const;

		Position pickFair();


		//////////////////////////////////////////////////////////////////////////
		// Statics 
		//////////////////////////////////////////////////////////////////////////

		static ofColor COLOR_BOARD[2];
		static ofColor COLOR_VALID;
		static ofColor COLOR_START;
		static ofColor COLOR_ATTACK;
		static ofColor COLOR_ATTACK2;

		static ofVec2f gBoardPos;
		static float gBoardScale;

		static void drawRect(const Position& pos) {
			ofRect(b2s(pos), gBoardScale, gBoardScale);
		}
		static ofVec2f b2s(float x, float y) {
			return gBoardPos + ofVec2f(x * gBoardScale, y * gBoardScale);
		}
		static ofVec2f b2s(const Position& pos) {
			return gBoardPos + ofVec2f(pos.x * gBoardScale, pos.y * gBoardScale);
		}
		static ofVec2f b2s(const ofVec2f& pos) {
			return gBoardPos + ofVec2f(pos.x * gBoardScale, pos.y * gBoardScale);
		}
		static ofVec2f s2b(const ofVec2f& pos) {
			return ofVec2f((pos.x - gBoardPos.x) / gBoardScale, ((pos.y - gBoardPos.y) / gBoardScale));
		}
		static Position s2b(float x, float y) {
			return Position((x - gBoardPos.x) / gBoardScale, ((y - gBoardPos.y) / gBoardScale));
		}

		//////////////////////////////////////////////////////////////////////////
		// Internals
		//////////////////////////////////////////////////////////////////////////

		vector<Piece*> mKings;

		void initDefaultSetup();

		void capturePiece(Piece* piece, int color) {
			removePiece(piece);
			piece->bDead = true;
			mCapturedPieces[color].push_back(piece);
		}
		void unCapturePiece(Piece* piece, int color) {
			addPiece(piece, piece->mPos);
			piece->bDead = false;
			for (PieceList::iterator it = mCapturedPieces[color].begin(); it != mCapturedPieces[color].end(); ++it) {
				if (*it == piece) {
					mCapturedPieces[color].erase(it);
					break;
				}
			}
		}

		int getScore(int color) {
			int score = 0;
			for (PieceList::iterator it = mCapturedPieces[color].begin(); it != mCapturedPieces[color].end(); ++it) {
				score += (*it)->getValue();
			}
			return score;
		}

		void addPiece(Piece* piece, Position pos);
		void removePiece(Piece* piece);

		shared_ptr<SlideInfo> addSlide(const chess::Maneuver& mve, Piece* piece, int startFrame, int endFrame);
		void addSlide(shared_ptr<SlideInfo>& info);
		void removeSlide(shared_ptr<SlideInfo>& info);

		Piece* spawn(const string& name, int color);
		Piece* spawn(char name, int color);

		void recalc(int color, const Position& from, const Position& to);

		int getNearestScore(const Position& from, const Position& to);
		void calcNearest(const Position& from);

	protected:
		vector< shared_ptr<SlideInfo> > mSlides;
		list< shared_ptr<CmdInfo> > mQueueCmds;
		list< shared_ptr<AddOverCmdInfo> > mKeepCmds;
		vector< shared_ptr<CmdInfo> > mUndoCmds;
		vector< vector<Piece*> > mBoard;
		vector<PieceList> mPlayerPieces;
		vector<PieceList> mCapturedPieces;
		list<LightInfo> mLightUps;
		int mLastCalc;
		string mLayout;

		PieceArr mAll;
		int mFrame;

		Position mNearestTo;
		vector< vector <int> > mNearestGrid;
	};  
};
