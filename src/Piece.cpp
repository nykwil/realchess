#include "Piece.h"
#include "Board.h"
#include "Gui.h"

using namespace chess;

// Pieces' values
const int PAWN_VALUE    = 30;	// 8x
const int ROOK_VALUE    = 90;	// 2x
const int KNIGHT_VALUE  = 85;	// 2x
const int BISHOP_VALUE  = 84;	// 2x
const int QUEEN_VALUE   = 300;	// 1x

Piece::Piece(int color) : mColor(color), mMoveCount(0), bDead(true), bSelected(false)
{
	mCooldownMult = 1.f;
	mPos.set(0, 0);
	mCooldown = 0;
	mBehavior = 0;
	bDirty = true;
}

void Piece::setup(const string& setName)
{
	char col = 'w';
	if (mColor == 1)
		col = 'b';
	else if (mColor == 2)
		col = 'g';
	
	if (mImage.loadImage(ofToDataPath(setName + string("/") + col + getAscii() + ".png"))) {
		mImage.resize(Board::gBoardScale, Board::gBoardScale);
	}
	buildRingMesh(mOutMesh, Board::gBoardScale * COOL_IN_RING, Board::gBoardScale * COOL_OUT_RING);
}

bool Piece::addPosition(Board* board, const Position& pos)
{
	if (!board->inBounds(pos))
		return false;

	Piece* pie = board->at(pos);
	if (pie == 0) {
		mMoves.push_back(pos);
		return true;
	}
	else {
		if (pie->mColor == mColor)
			mBlocks.push_back(pos);
		else
			mAttacks.push_back(pos);

		return false;
	}
}

void Piece::shootPath(Board* board, Position pos, Direction d)
{
	pos.move(d);
	while (addPosition(board, pos)) 
		pos.move(d);
}


void Pawn::calculate(Board* board)
{
	mBlocks.clear(); mMoves.clear(); mAttacks.clear();

	int offset = mColor == WHITE ? 1 : -1;
	
	Position pos;
	pos.set(mPos.x - 1, mPos.y + offset);
	if (board->at(pos) && board->at(pos)->mColor != mColor) {
		mAttacks.push_back(pos);
	}

	pos.set(mPos.x + 1, mPos.y + offset);
	if (board->at(pos) && board->at(pos)->mColor != mColor) {
		mAttacks.push_back(pos);
	}

	pos.set(mPos.x, mPos.y + offset);
	if (board->inBounds(pos)) { 
		Piece* pie = board->at(pos);
		if (pie)
			mBlocks.push_back(pos);
		else {
			mMoves.push_back(pos);
			if (mMoveCount == 0) {
				pos.set(pos.x, mPos.y + offset * 2);         
				if (board->inBounds(pos)) { 
					Piece* pie = board->at(pos);
					if (pie)
						mBlocks.push_back(pos);
					else 
						mMoves.push_back(pos);
				}
			}
		}
	}
}

void Piece::draw()
{
	ofPushMatrix();
	ofTranslate(Board::b2s(ofVec2f(mPos.x + 0.5f, mPos.y + 0.5f))); // make t
	drawPiece(255);
	ofPopMatrix();
}

void Piece::drawPiece(int alpha)
{
	if (bSelected) {
		ofSetColor(Board::COLOR_START);
		ofCircle(0, 0, Board::gBoardScale * 0.5f); 
	}
	ofSetColor(255, alpha);
	mImage.draw(-mImage.getWidth() * 0.5f, -mImage.getHeight() * 0.5f);
}

void Piece::drawRing()
{
	ofPushMatrix();
	ofTranslate(Board::b2s(ofVec2f(mPos.x + 0.5f, mPos.y + 0.5f))); // make t
	ofSetColor(OUT_RING_COLOR);
	mOutMesh.draw();
	ofSetColor(IN_RING_COLOR);
	mInMesh.draw();
	ofPopMatrix();
}

bool Piece::isAttacking(const Position& pos) const
{
	for (PosList::const_iterator it = mAttacks.begin(); it != mAttacks.end(); ++it) {
		if (*it == pos)
			return true;
	}
	return false;
}

Position Piece::getNearestPos(const ofVec2f& pos, const Position& startPos, float random)
{
	// make sure the nearest move is closer then the current position
	Position minpos = startPos; 
	float mindist = pos.distance(ofVec2f(startPos.x, startPos.y));

	for (PosList::iterator it = mMoves.begin(); it != mMoves.end(); ++it) {
		float dist = pos.distance(ofVec2f(it->x, it->y)) + ofRandom(random);
		if (dist < mindist) {
			mindist = dist;
			minpos = *it;
		}
	}
	for (PosList::iterator it = mAttacks.begin(); it != mAttacks.end(); ++it) {
		float dist = pos.distance(ofVec2f(it->x, it->y)) + ofRandom(random); 
		if (dist < mindist) {
			mindist = dist;
			minpos = *it;
		}
	}
	return minpos;
}

void Piece::added(const Position& pos, int frame)
{
	mPos = pos;
	mMoveCount = 0;
	mCooldown = frame + getCooldownTime();
	bDead = false;
}

void Piece::unadded(const Position& pos, int frame)
{
	mCooldown = 0;
	bDead = true;
}

void Piece::moved(const Position& pos, int frame)
{
	++mMoveCount; 
	mPos = pos;
	mCooldown = frame + getCooldownTime();
}

void Piece::unmoved(const Position& pos, int frame)
{
	--mMoveCount; 
	mPos = pos;
	mCooldown = 0;
}

//////////////////////////////////////////////////////////////////////////

void Knight::calculate(Board* board)
{
	mBlocks.clear(); mMoves.clear(); mAttacks.clear();

	addPosition(board, mPos + Position(1, -2));
	addPosition(board, mPos + Position(2, -1));
	addPosition(board, mPos + Position(2, 1));
	addPosition(board, mPos + Position(1, 2));
	addPosition(board, mPos + Position(-1, 2));
	addPosition(board, mPos + Position(-2, 1));
	addPosition(board, mPos + Position(-2, -1));
	addPosition(board, mPos + Position(-1, -2));
}

void Rook::calculate(Board* board)
{
	mBlocks.clear(); mMoves.clear(); mAttacks.clear();

	shootPath(board, mPos, NORTH);
	shootPath(board, mPos, EAST);
	shootPath(board, mPos, SOUTH);
	shootPath(board, mPos, WEST);
}

void Bishop::calculate(Board* board)
{
	mBlocks.clear(); mMoves.clear(); mAttacks.clear();

	shootPath(board, mPos, NORTH_EAST);
	shootPath(board, mPos, SOUTH_EAST);
	shootPath(board, mPos, SOUTH_WEST);
	shootPath(board, mPos, NORTH_WEST);
}

void Queen::calculate(Board* board)
{
	mBlocks.clear(); mMoves.clear(); mAttacks.clear();

	shootPath(board, mPos, NORTH);
	shootPath(board, mPos, EAST);
	shootPath(board, mPos, SOUTH);
	shootPath(board, mPos, WEST);
	shootPath(board, mPos, NORTH_EAST);
	shootPath(board, mPos, SOUTH_EAST);
	shootPath(board, mPos, SOUTH_WEST);
	shootPath(board, mPos, NORTH_WEST);
}

void King::calculate(Board* board)
{
	mBlocks.clear(); mMoves.clear(); mAttacks.clear();

	addPosition(board, mPos + Position(0, -1));
	addPosition(board, mPos + Position(1, -1));
	addPosition(board, mPos + Position(1, 0));
	addPosition(board, mPos + Position(1, 1));
	addPosition(board, mPos + Position(0, 1));
	addPosition(board, mPos + Position(-1, 1));
	addPosition(board, mPos + Position(-1, 0));
	addPosition(board, mPos + Position(-1, -1));

	//@TODO castle
}

//////////////////////////////////////////////////////////////////////////

void SeekerBee::setup(const Position& offset)
{
	mOffset = offset;
}

bool AttackBee::calcForce(Position& force, Piece* piece, Board* board)
{
	if (!piece->mAttacks.empty()) {
		// get the best attack
		force = piece->mAttacks[0]; 
		int bval = board->at(force)->getValue();
		for (int i = 1; i < piece->mAttacks.size(); ++i) {
			Position pos = piece->mAttacks[i];
			int val = board->at(pos)->getValue();
			if (bval > val) {
				force = pos;
				bval = val;
			}
		}
		return true;
	}
	return false;
}

bool HunterBee::calcForce(Position& force, Piece* piece, Board* board)
{
	if (AttackBee::calcForce(force, piece, board))
		return true;
	else {
		force = piece->mPos;
		for (int i = 0; i < board->mKings.size(); ++i) {
			if (i != piece->mColor && board->mKings[i]) {
				force = piece->getNearestPos(board->mKings[i]->mPos, force);
			}
		}
		return (force != piece->mPos);
	}
	return false;
}

bool SeekerBee::calcForce(Position& force, Piece* piece, Board* board)
{
	if (AttackBee::calcForce(force, piece, board))
		return true;
	else {
		int index = piece->mMoveCount + 1;
		if (mLoop) 
			index = (index % mPositions.size()) + mLoopPoint;
		if (index < mPositions.size()) {
			Position pos = board->modPos(mPositions[index] + mOffset);
			force = piece->getNearestPos(pos, 0);
			return true;  // seekers always return true if looping makes them stall
		}
		else 
			return false;
	}
	return false;
}

