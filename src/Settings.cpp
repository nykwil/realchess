#include "Settings.h"
#include "TouchPlayer.h"
#include "Board.h"
#include "Piece.h"
#include "RealChess.h"
#include "Loader.h"
#include "Timer.h"
#include "Bank.h"

using namespace chess;

bool TouchPlayer::gSnap = false;
float TouchPlayer::gMaxCharge = 100.f;
int TouchPlayer::gMaxTokens = 3;
ofRectangle TouchPlayer::gChargeRect(0, 0, 100, 20);

ofVec2f Board::gBoardPos(0.f, 100.f);
float Board::gBoardScale = 96.f;
ofColor Board::COLOR_VALID(40, 40, 40, 200);
ofColor Board::COLOR_START(60, 60, 40, 200);
ofColor Board::COLOR_ATTACK(150, 20, 20, 100);
ofColor Board::COLOR_ATTACK2(240, 10, 10, 100);
ofColor Board::COLOR_BOARD[2] = { ofColor(10,10,10), ofColor(200, 200, 200) };

float Piece::COOL_OUT_RING = 0.8f;
float Piece::COOL_IN_RING = 0.5f;
ofColor Piece::IN_RING_COLOR = ofColor(123,21,32,40);
ofColor Piece::OUT_RING_COLOR = ofColor(43,121,21,50);

int Pawn::VALUE = 30;
int Pawn::COOLDOWN = 30;
int Rook::VALUE = 90;
int Rook::COOLDOWN = 30;
int Knight::VALUE = 85;
int Knight::COOLDOWN = 30;
int Bishop::VALUE = 84;
int Bishop::COOLDOWN = 30;
int Queen::VALUE = 300;
int Queen::COOLDOWN = 30;
int King::VALUE = ((Pawn::VALUE * 8) + (Rook::VALUE * 2) + (Knight::VALUE * 2) + (Bishop::VALUE * 2) + Queen::VALUE + WIN_VALUE);
int King::COOLDOWN = 30;
int Coin::VALUE = 100;
float Piece::SPEED = 0.05f;

ofColor Bank::COLOR_BG = ofColor::aliceBlue;

GameplaySettings::GameplaySettings() 
{
	// gameplay
	newProperty("touchplayer_snap", TouchPlayer::gSnap);
	newProperty("touchplayer_maxcharge", TouchPlayer::gMaxCharge);
	newProperty("touchplayer_maxtokens", TouchPlayer::gMaxTokens);

	newProperty("pawn_value", Pawn::VALUE);
	newProperty("pawn_cooldown", Pawn::COOLDOWN);
	newProperty("rook_value", Rook::VALUE);
	newProperty("rook_cooldown", Rook::COOLDOWN);
	newProperty("knight_value", Knight::VALUE);
	newProperty("knight_cooldown", Knight::COOLDOWN);
	newProperty("bishop_value", Bishop::VALUE);
	newProperty("bishop_cooldown", Bishop::COOLDOWN);
	newProperty("queen_value", Queen::VALUE);
	newProperty("queen_cooldown", Queen::COOLDOWN);
	newProperty("king_cooldown", King::COOLDOWN);

	newProperty("piece_speed", Piece::SPEED);
}

UISettings::UISettings() 
{
	// look
	newProperty("touchplayer_position", TouchPlayer::gChargeRect.position);
	newProperty("touchplayer_width", TouchPlayer::gChargeRect.width);
	newProperty("touchplayer_height", TouchPlayer::gChargeRect.height);

	newProperty("board_color_black", Board::COLOR_BOARD[0]);
	newProperty("board_color_white", Board::COLOR_BOARD[1]);
	newProperty("board_color_valid", Board::COLOR_VALID);
	newProperty("board_color_start", Board::COLOR_START);
	newProperty("board_color_attack", Board::COLOR_ATTACK);
	newProperty("board_color_attack2", Board::COLOR_ATTACK2);
	newProperty("bank_color_bg", Bank::COLOR_BG);
	newProperty("piece_cool_out_ring", Piece::COOL_OUT_RING);
	newProperty("piece_cool_in_ring", Piece::COOL_IN_RING);
	newProperty("piece_in_ring_color", Piece::IN_RING_COLOR);
	newProperty("piece_out_ring_color", Piece::OUT_RING_COLOR);

	newProperty("gui_textcolour", GuiWidget::gTextColour);
	newProperty("gui_downcolour", GuiWidget::gDownColour);
	newProperty("gui_bordercolour", GuiWidget::gBorderColour);
	newProperty("gui_bgcolour", GuiWidget::gBgColour);
	newProperty("gui_highlightcolour", GuiWidget::gHighlightColour);
	newProperty("gui_dialogbg", Gui::gDialogBg);
	newProperty("gui_smallfontsize", Gui::mSmallFontSize);
	newProperty("gui_mediumfontsize", Gui::mMediumFontSize);
	newProperty("gui_largefontsize", Gui::mLargeFontSize);
}

chess::Settings::Settings() : gui(0, 0, 400, 400)
{
	gui.mCanvasBg = ofColor::azure;
	gui.addWidget(shared_ptr<GuiSlider>(new GuiSlider(20, 20, 100, 20, 0, 10, TouchPlayer::gMaxCharge, "Max Charge")));
	gui.addWidgetDown(shared_ptr<GuiSlider>(new GuiSlider(0, 0, 100, 20, 0, 10, TouchPlayer::gMaxTokens, "Max Tokens")));
	gui.addWidgetDown(shared_ptr<GuiSlider>(new GuiSlider(0, 0, 100, 20, 16.66666666, 1000, Timer::mDesiredFrameTime, "FrameTime")));
	gui.addWidgetDown(shared_ptr<GuiButton>(new GuiButton(0, 0, 100, 50, "Save", this, &chess::Settings::onSave)));
	gui.addWidgetDown(shared_ptr<GuiButton>(new GuiButton(0, 0, 100, 50, "Load", this, &chess::Settings::onLoad)));
}

void chess::Settings::onSave()
{
	Loader::save(ofToDataPath("gameplay_settings.xml"), &mGameplay);
	Loader::save(ofToDataPath("ui_settings.xml"), &mUI);
}

void chess::Settings::onLoad()
{
	Loader::load(ofToDataPath("gameplay_settings.xml"), &mGameplay);
	Loader::load(ofToDataPath("ui_settings.xml"), &mUI);
}
