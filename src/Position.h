#pragma once

#include "pch.h"

using namespace std;

namespace chess
{
	class Piece;

	enum Color
	{
		WHITE,
		BLACK
	};

	enum Direction
	{
		NORTH,
		NORTH_EAST,
		EAST,
		SOUTH_EAST,
		SOUTH,
		SOUTH_WEST,
		WEST,
		NORTH_WEST
	};

	class Position
	{
	public:
		Position(int x = 0, int y = 0);
		
		void set(string id);
		string getId() const;

		void move(int x, int y);
		void move(Direction d);
		void set(int x, int y);

		bool operator==(const Position& other) const;
		bool operator!=(const Position& other) const; 
		const Position operator+(const Position& other) const;    

		int x;
		int y;

		static Position INVALID;
	};

	class Maneuver
	{
	public: 
		Maneuver() {}
		Maneuver(const Position& from, const Position& to) : mFrom(from), mTo(to) {}

		Position mFrom;
		Position mTo;
	};

	typedef std::vector<Position> PosList;
	typedef std::vector<Maneuver> MoveList;
};

