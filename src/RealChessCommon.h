#pragma once

#include "PropertyContainer.h"
#include "Position.h"

namespace StringConverter {
	void fromString(vector<chess::Position>& f, const string& str);
	string toString(const vector<chess::Position>& vec);
	void fromString(chess::Position& f, const string& str);
	string toString(const chess::Position& vec);
};

class Timer
{
public:
	static void setFrameRate(float fps) {
		mDesiredFrameTime = (unsigned long)(1.0f / fps * 1000.0f);
	}

	static int getDesiredFrameTime() {
		return mDesiredFrameTime;
	}

	static int mDesiredFrameTime;
};

struct PlayerData 
{
	string mIp;
	int mPort;
	string mClientName;
	bool mServerMode;
	string mJump;
	string mLevelFile;
	string mBank;
	int mCoins;
	int mStartingCurrency;
};

struct PlayerXml : public PropertyObject, public PlayerData
{
	PlayerXml() {
		newProperty("Ip", mIp);
		newProperty("Port", mPort);
		newProperty("ClientName", mClientName);
		newProperty("ServerMode", mServerMode, false);
		newProperty("Jump", mJump);
		newProperty("LevelFile", mLevelFile, string("data/levels/testlevel.xml"));
		newProperty("Bank", mBank, string("pnbq"));
		newProperty("Coins", mCoins, 0);
		newProperty("StartingCurrency", mStartingCurrency, 100);
	}
	virtual string getType() { return "Player"; }
};

class Game
{
public:
	static PlayerXml mPlayerInfo;
	static void loadPlayer(const string& filename);
	static bool isDebugMode();
};

