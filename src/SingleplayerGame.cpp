#include "SingleplayerGame.h"
#include "Loader.h"
#include "Gui.h"
#include "RealChess.h"
#include "CmdInfo.h"

using namespace chess;

TFactory<AIPieceXml> factBehavior("AIPieceXml");
TFactory<WaveXml> factRunner("Wave");
TFactory<GameXml> factGame("Game");

int gPreSpawn = 0;

//////////////////////////////////////////////////////////////////////////

SingeplayerGameState::SingeplayerGameState()
{
	mBoard.setup(2, 8, 8, "");
	mBank.setup(0, &mBoard);
	mPlayer.setup(0, &mBoard);

	ofVec2f p = Board::b2s(0, mBoard.getHeight() + 1);
	gui = make_shared<GuiCanvas>(p.x, p.y, ofGetWidth(), 100);
	gui->addWidget(shared_ptr<GuiButton>(new GuiButton(350, 0, 100, 40, "Quit", this, &SingeplayerGameState::onQuit)));
	gui->addWidget(mText = make_shared<GuiLabel>(300, 10, "Frame: "));
}

void SingeplayerGameState::update(float dt)
{
	mPlayer.update();
	mBank.update();

	mText->setText("Frame: " + ofToString(mBoard.getFrame()));

	if (mData.mSpawnMoneyFrame != 0 && mBoard.getFrame() % mData.mSpawnMoneyFrame == 0) {
		mBoard.cmdAdd('c', 2, mBoard.pickFair());
	}

	AddPiece ap;
	while (mBank.getAdd(ap)) {
		mBoard.cmdAdd(ap.mPieceName, ap.mColor, ap.mPos);
	}

	Maneuver mve;
	while (mPlayer.getMove(mve)) {
		if (mBoard.isValidMove(mPlayer.mColor, mve)) {
			mBoard.cmdSlide(mve);
			--mPlayer.mTokens;
		}
	}

	// @todo should be timed somehow
	mBoard.advance();

	if (mBoard.getPlayerStatus(0) == Dead) {
		cout << "You loosed" << endl;
		RealManager::instance.onQuitGame();
	}
}

void SingeplayerGameState::draw()
{
	gui->draw();
	mBoard.draw();
	mBank.draw();
	mPlayer.draw();
}

void SingeplayerGameState::enter()
{
	addChild(&mBank);
	addChild(&mPlayer);
	addChild(gui.get());

	if (Loader::load(Game::mPlayerInfo.mLevelFile, &mData)) {
		// mInfo = mNewLevel; @todo create a new level
	}
	
	mData.mWaves[0]->setup(&mBoard, 1);
	mBank.setup(Game::mPlayerInfo.mBank, Game::mPlayerInfo.mStartingCurrency);
	mBoard.cmdAdd('k', 0, mData.mKingStart);
}

void SingeplayerGameState::exit()
{
	removeChild(gui.get());
	removeChild(&mPlayer);
	removeChild(&mBank);
}

void SingeplayerGameState::onQuit()
{
	RealManager::instance.onQuitGame();
}

AIPieceXml* EditorGameState::createPiece(Position p)
{
	AIPieceXml* be = new AIPieceXml();
	SeekerBee* seeker = new SeekerBee();
	seeker->mLoop = true;// @todo
	seeker->mPositions.push_back(p);
	be->mStartPos = p;
	be->mBee = seeker;
	be->mOffsets.push_back(Position(0, 0));
	be->mPieceName = ofToString(mBankSelected->getAscii());
	be->mSpawnFrame = mBoard.getFrame();
	be->setup(&mBoard, 1);
	return be;
}

//////////////////////////////////////////////////////////////////////////

void AIPieceXml::setup(Board* board, int color)
{
	Piece* piece = board->spawn(mPieceName, color);
	piece->mCooldownMult = mCooldownMult;

	Position offset = mOffsets.empty() ? Position() : mOffsets[rand() % mOffsets.size()];
	piece->mBehavior = mBee;

	Position pos = board->modPos(mStartPos + offset);

	addCmd = make_shared<AddOverCmdInfo>(mSpawnFrame, piece, pos);

	board->keepCmd(addCmd);

	mPiece = piece;
	mBee = static_cast<SeekerBee*>(piece->mBehavior);

	// @TODO add the behavior to the UI
}

void AIPieceXml::destroy(Board* board)
{
	board->removePiece(mPiece);
	board->removeKeepCmd(addCmd);
}

void AIPieceXml::drawNext(Board* board)
{
	Position force;
	if (mBee->calcForce(force, mPiece, board)) {
		ofSetColor(Board::COLOR_VALID);
		board->drawRect(force);
	}
}

void AIPieceXml::editPos(const Position& pos)
{
	int index = mPiece->mMoveCount + 1;
	int i = 0;
	while (index >= mBee->mPositions.size()) {
		mBee->mPositions.push_back(mBee->mPositions[i % mBee->mPositions.size()]);
		++i;
	}
	if (mBee->mPositions.size() <= index) {
		mBee->mPositions.push_back(pos);
	}
	else {
		mBee->mPositions[index] = pos;
	}
}

void WaveXml::setup(Board* board, int color)
{
	for (vector<AIPieceXml*>::iterator it = mBehaviors.begin(); it != mBehaviors.end(); ++it) {
		(*it)->setup(board, color);
	}
}
