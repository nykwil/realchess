#include "RealChess.h"
#include "Board.h"
#include "Player.h"
#include "MultiplayerGame.h"
#include "SingleplayerGame.h"
#include "TouchPlayer.h"
#include "Settings.h"
#include "Loader.h"
#include "Screens.h"
#include "ServerGame.h"

using namespace chess;

RealManager RealManager::instance;

RealManager::RealManager() : 
	mSettings(0),
	mMainMenuState(0),
	mServer(0)
{
	bDebug = true;
}

RealManager::~RealManager()
{
	// pop states
	delete mMainMenuState;
	delete mServer;
	delete mSettings;
}

void RealManager::setup()
{
	mSettings = new Settings();
	mSettings->onLoad();
	Gui::setup();

	mMainMenuState = new MainMenuState();
	mServer = new ServerGame();

	mSettings->gui.hide();
	addChild(&mSettings->gui);
	
	if (Game::mPlayerInfo.mJump == "Editor") {
		onEditGame();
	}
	else if (Game::mPlayerInfo.mJump == "Client")	{
		onClientGame();
	}
	else {
		pushState(mMainMenuState);
	}
}

void RealManager::changeState(GameState *gameState)
{
	if (!mStates.empty()) removeChild(static_cast<TouchState*>(mStates.back()));
	GameManager::changeState(gameState);
	if (!mStates.empty()) addChild(static_cast<TouchState*>(mStates.back()));
}

void RealManager::pushState(GameState *gameState)
{
	if (!mStates.empty()) removeChild(static_cast<TouchState*>(mStates.back()));
	GameManager::pushState(gameState);
	if (!mStates.empty()) addChild(static_cast<TouchState*>(mStates.back()));
}

void RealManager::popState()
{
	if (!mStates.empty()) removeChild(static_cast<TouchState*>(mStates.back()));
	GameManager::popState();
	if (!mStates.empty()) addChild(static_cast<TouchState*>(mStates.back()));
}

void RealManager::draw()
{
	GameManager::draw();
	ofDisableDepthTest();
	mSettings->gui.draw();
}

bool RealManager::isServerOn()
{
	return mServer->isThreadRunning();
}

void RealManager::toggleServer()
{
	if (mServer->isThreadRunning()) {
		mServer->stopThread();
	}
	else {
		mServer->setup(Game::mPlayerInfo.mPort);
		mServer->startThread(false, true);
	}
}

void RealManager::toggleSettings()
{
	mSettings->gui.toggleEnabled();
}

void RealManager::onClientGame()
{
	mGameState = new ClientPreState();
	pushState(mGameState);
}

void RealManager::onServerGame()
{
	mGameState = new ServerPreState();
	pushState(mGameState);
}

void RealManager::onLocalGame()
{
	mGameState = new RealPlayState();
	pushState(mGameState);
}

void RealManager::onVanillaGame()
{
	mGameState = new VanillaPlayState();
	pushState(mGameState);
}

void RealManager::onEditGame()
{
	mGameState = new EditorGameState();
	pushState(mGameState);
}

void RealManager::onSingleGame()
{
	mGameState = new SingeplayerGameState();
	pushState(mGameState);
}

void RealManager::onQuitGame()
{
	popState();
	delete mGameState;
}

//////////////////////////////////////////////////////////////////////////

void VanillaPlayState::enter()
{
	mBoard.setup(2, 8, 8, "");
	mBoard.initDefaultSetup();

	mPlayers.clear();
	// Read settings

	touch = new TouchPlayer();
	touch->setup(0, &mBoard);
	mPlayers.push_back(touch);
	addChild(touch);

	Player* ai = new AIPlayer();
	ai->setup(1, &mBoard);
	mPlayers.push_back(ai);

	mTurn = 0;
}

void VanillaPlayState::exit()
{
	removeChild(touch);
	for (int i = 0; i < mPlayers.size(); ++i)
		delete mPlayers[i];
}

void VanillaPlayState::update(float dt)
{
	Maneuver mve;

	if (mPlayers[mTurn]->getMove(mve)) {
		mBoard.cmdMove(mve);

		Status status = mBoard.getPlayerStatus(mTurn);

		switch(status)
		{
		case Checkmate:
			printf("Checkmate\n");
			break;
		case Stalemate:
			printf("Stalemate\n");
			break;
		}
		mTurn = (mTurn + 1) % 2; // @todo PLAYERS;
	}
}

void VanillaPlayState::draw()
{
	mBoard.draw();
	touch->draw();
}

//////////////////////////////////////////////////////////////////////////

void RealPlayState::enter()
{
	mBoard.setup(2, 8, 8, "");

	mPlayers.clear();
	// Read settings
	TouchPlayer* touch1 = new TouchPlayer();
	touch1->setup(0, &mBoard);
	TouchPlayer* touch2 = new TouchPlayer();
	touch2->setup(1, &mBoard);
	addChild(touch1);
	addChild(touch2);

	mPlayers.push_back(touch1);
	mPlayers.push_back(touch2);

	mBoard.initDefaultSetup();
}

void RealPlayState::exit()
{
	for (int i = 0; i < mPlayers.size(); ++i) {
		removeChild(mPlayers[i]);
		delete mPlayers[i];
	}
}

void RealPlayState::update(float dt)
{
	for (int i = 0; i < mPlayers.size(); ++i) {
		mPlayers[i]->update();
		Maneuver mv;
		while (mPlayers[i]->getMove(mv)) {
			mBoard.cmdSlide(mv);
		}
	}

	// @TODO should be timed somehow
	mBoard.advance();
}

void RealPlayState::draw()
{
	mBoard.draw();
	for (int i = 0; i < mPlayers.size(); ++i) 
		mPlayers[i]->draw();
}
