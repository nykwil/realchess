#pragma once

#include "GameManager.h"
#include "Touchable.h"
#include "Board.h"
#include "RealChessCommon.h"

namespace chess
{
	class Player;
	class Board;
	class TouchPlayer;
	class Settings;
	class ServerGame;
};

class Loader;

class TouchState : public GameState, public Touchable
{
public:
};

class VanillaPlayState : public TouchState
{
public:
	virtual void enter();
	virtual void exit();

	virtual void update(float dt);
	virtual void draw();

	virtual void pause() {};
	virtual void resume() {};

	chess::TouchPlayer* touch;
	vector<chess::Player*> mPlayers;

	chess::Board mBoard;
	int mTurn;
};

class RealPlayState : public TouchState
{
public:
	virtual void enter();
	virtual void exit();

	virtual void update(float dt);
	virtual void draw();

	virtual void pause() {};
	virtual void resume() {};

	vector<chess::Player*> mPlayers;
	chess::Board mBoard;
};

class RealManager : public GameManager, public Touchable
{
public:
	RealManager();
	virtual ~RealManager();
	virtual void setup();

	virtual void changeState(GameState *gameState);
	virtual void pushState(GameState *gameState);
	virtual void popState();

	virtual void toggleServer();
	bool isServerOn();

	virtual void draw();

	void onClientGame();
	void onServerGame();
	void onLocalGame();
	void onVanillaGame();
	void onSingleGame();
	void onEditGame();
	void onQuitGame();

	TouchState* mGameState;
	TouchState* mMainMenuState;

	chess::ServerGame* mServer;
	chess::Settings* mSettings;

	static RealManager instance;

	bool bDebug;
	void toggleSettings();
};
