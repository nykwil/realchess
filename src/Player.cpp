#include "Player.h"
#include <cstdlib>
#include <cstdio>
#include <list>
#include <ctime>
#include <cctype>
#include <cstring>
#include <iostream>
#include "Gui.h"

using namespace std;

namespace chess {

// @TODO deprecate 
int TOGGLE_COLOR(int x)
{
	return x ? 0 : 1;
}

bool HumanPlayer::getMove(Maneuver& move) 
{
	MoveList regulars, nulls;
	char * input;

	for(;;) {
		printf(">> ");

		if((input = readInput()) == NULL) {
			ofLogError() << "Could not read input" << endl;
			continue;
		}

		if(!processInput(input, move)) {
			ofLogError() << "Error while parsing input" << endl;
			continue;
		}

		if(!mBoard->isValidMove(mColor, move)) {
			ofLogError() << "Invalid move" << endl;
			continue;
		}

		printf("\n");
		break;
	}

	return true;
}

char* HumanPlayer::readInput(void)
{
	int buffsize = 8, c, i;
	char * buffer, * tmp;

	// allocate buffer
	buffer = reinterpret_cast<char*>(malloc(buffsize));
	if(!buffer)	{
		return NULL;
	}

	// read one line
	for(i = 0; (c = fgetc(stdin)) != '\n'; i++)	{
		if(i >= buffsize - 1) {
			if((buffsize << 1) < buffsize) {
				fprintf(stderr, "HumanPlayer::readInput(): " \
					"Someone is trying a buffer overrun.\n");
				free(buffer);
				return NULL;
			}
			tmp = reinterpret_cast<char*>(realloc(buffer, buffsize<<1));
			if(!tmp) {
				free(buffer);
				return NULL;
			}
			buffer = tmp;
			buffsize <<= 1;
		}
		buffer[i] = c;
	}

	// terminate buffer
	buffer[i] = '\0';
	return buffer;	
}

bool HumanPlayer::processInput(char * buf, Maneuver & move)
{
	int i = 0;
	// skip whitespace
	while(true) {
		if(buf[i] == '\0') {
			free(buf);
			return false;
		}
		else if(!isspace(buf[i])) {
			break;
		}
		else {
			i++;
		}
	}

	if(strncmp(&buf[i], "quit", 4) == 0)
		exit(0);

	// convert from sth. like "b1c3"	
	for(int j = 0; j < 2; j++) {
		int l, n;
		l = buf[i++];
		n = buf[i++];
		if(l >= 'a' && l <= 'h') {
			l = l - 'a';
		}
		else if(l >= 'A' && l <= 'H') {
			l = l - 'A';
		}
		else {
			free(buf);
			return false;
		}
		if(n >= '1' && n <= '8') {
			n = n - '1';
		}
		else {
			free(buf);
			return false;
		}
		if(j == 0)
			move.mFrom = Position(n * 8 + l);
		else 
			move.mTo = Position(n * 8 + l);
	}

	free(buf);
	return true;
}

AIPlayer::AIPlayer() 
{
	mDepth = 2;
	srand((unsigned int)time(NULL));
}

bool AIPlayer::getMove(Maneuver& move)
{
	MoveList candidates;
	bool quiescent = false;

	// first assume we are loosing
	int best = -King::VALUE;

	// get all moves
	MoveList moves;
	mBoard->getMoves(mColor, moves);

	// loop over all moves
	for(MoveList::iterator it = moves.begin(); it != moves.end(); ++it) {
		quiescent = mBoard->hasPosition(it->mTo);

		// execute move
		mBoard->cmdMove(*it);

		// check if own king is vulnerable now
		if(!mBoard->isVulnerable(mColor, mBoard->mKings[mColor]->mPos)) {

			// recursion
			int tmp = -evalAlphaBeta(TOGGLE_COLOR(mColor), mDepth - 1, -WIN_VALUE, -best, quiescent);
			if(tmp > best) {
				best = tmp;
				candidates.clear();
				candidates.push_back(*it);
			}
			else if(tmp == best) {
				candidates.push_back(*it);
			}
		}

		// undo move 
		mBoard->undo();
	}

	// loosing the game?
	if(best < -WIN_VALUE) {
		return false;
	}
	else {
		// select random move from candidate moves
		move = candidates[rand() % candidates.size()];
		return true;
	}
}

int AIPlayer::evalAlphaBeta(int color, int depth, int alpha, int beta, bool quiescent)
{
	// @TODO refactor
	if(depth <= 0 && !quiescent) {
		if(color)
			return -mBoard->evaluateBoard(0);
		else
			return +mBoard->evaluateBoard(0);
	}

	// first assume we are loosing
	int best = -WIN_VALUE;

	// get all moves
	MoveList moves;
	mBoard->getMoves(color, moves);

	// loop over all moves
	for(MoveList::iterator it = moves.begin(); alpha <= beta && it != moves.end(); ++it) {
		quiescent = mBoard->hasPosition(it->mTo);

		// execute move
		mBoard->cmdMove(*it);

		// check if own king is vulnerable now
		if(!mBoard->isVulnerable(color, mBoard->mKings[color]->mPos)) {
				
			// recursion 'n' pruning
			int tmp = -evalAlphaBeta(TOGGLE_COLOR(color), depth - 1, -beta, -alpha, quiescent);
			if(tmp > best) {
				best = tmp;
				if(tmp > alpha) {
					alpha = tmp;
				}
			}
		}

		// undo move 
		mBoard->undo();
	}

	return best;
}

};
