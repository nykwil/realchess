#pragma once

#include "ofxNetwork.h"
#include "Board.h"
#include "RealChessCommon.h"

class Loader;

// must be a power of 2 and less then sizeof() any of the msgs bellow
#define MAX_MSG 32

#define MSG_READY		'r'
#define MSG_START		's'
#define MSG_MOVE		'm'
#define MSG_CONNECTED	'c'
#define MSG_ADDPIECE	'a'
#define MSG_ENDGAME		'e'

struct BaseMsg
{
	BaseMsg(char c) : mType(c) {}
	char mType;
};

struct TimeMsg : public BaseMsg
{
	TimeMsg(char c, int timeStamp) : BaseMsg(c), mTimeStamp(timeStamp) {}
	int mTimeStamp;
	char mColor;

protected:
	char fromX;
	char fromY;
	char toX;
	char toY;
};

struct ConnectedMsg : public BaseMsg
{
	ConnectedMsg(const char* name) : BaseMsg(MSG_CONNECTED) {
		strcpy_s(mName, MAX_MSG - 1, name);
	}
	char mName[MAX_MSG - 1];
};

struct ReadyMsg : public BaseMsg
{
	ReadyMsg(const char* name, bool ready) : BaseMsg(MSG_READY), bReady(ready) {
		strcpy_s(mName, name);
	}
	char mName[12];
	bool bReady;
};

struct StartGameMsg : public BaseMsg
{
	StartGameMsg(int color) : BaseMsg(MSG_START), mColor(color) {}
	int mColor;
};

struct EndGameMsg : public BaseMsg
{
	EndGameMsg(int loser) : BaseMsg(MSG_ENDGAME), mLooser(loser) {}
	int mLooser;
};

struct MoveMsg : public TimeMsg
{
	MoveMsg(char color, int timeStamp, const chess::Maneuver& move) :
		TimeMsg(MSG_MOVE, timeStamp)
	{
		mColor = color;
		fromX = move.mFrom.x;
		fromY = move.mFrom.y;
		toX = move.mTo.x;
		toY = move.mTo.y;
	}

	chess::Maneuver getMove() { return chess::Maneuver(chess::Position(fromX, fromY), chess::Position(toX, toY)); };
};

struct AddPieceMsg : public TimeMsg
{
	AddPieceMsg(char color, int timeStamp, char piece, const chess::Position& pos) : 
		TimeMsg(MSG_ADDPIECE, timeStamp)
	{
		mColor = color;
		fromX = piece;
		toX = pos.x;
		toY = pos.y;
	}

	char getPiece() { return fromX; }
	chess::Position getPosition() { return chess::Position(toX, toY); };
};

/*
union MessageUnion {
	char buffer[MAX_MSG];

	char type;
	TimeMsg timeMsg;
	ConnectedMsg connectedMsg;
	ReadyMsg readyMsg;
	StartGameMsg startGameMsg;
	EndGameMsg endGameMsg;
	MoveMsg moveMsg;
	AddPieceMsg addPieceMsg;
};
*/

namespace chess
{
struct PieceXml : public PropertyObject {
	virtual string getType() { return "Piece"; };
	PieceXml(char pieceName = ' ', int color = 0, Position pos = Position()) : mPieceName(pieceName), mColor(color), mPos(pos) {
		newProperty("PieceName", mPieceName);
		newProperty("Color", mColor);
		newProperty("Pos", mPos);
	}
	char mPieceName;
	int mColor;
	Position mPos;
};

struct BoardXml : public PropertyObject
{
	virtual string getType() { return "Board"; };
	BoardXml() {
		newContainer("Pieces", mPieces);
	}
	virtual ~BoardXml() { for (vector<PieceXml*>::iterator it = mPieces.begin(); it != mPieces.end(); ++it) delete *it; }
	vector<PieceXml*> mPieces;
};

class ServerGame : public ofThread
{
public:
	enum ServerState
	{
		SS_PRE,
		SS_WAIT,
		SS_GAME
	};
	ofxTCPServer mTcp;
	char buffer[TCP_MAX_MSG_SIZE];
	chess::Board mBoard;
	list<TimeMsg> mMsgs;

	map<int, string> mClientNames;

	int mClientsReady;

	void setState(ServerState state);
	ServerState mState;

	unsigned long mTimeLastFrame;
	unsigned long mTimeAcumulator;

	void setup(int port);

	virtual void threadedFunction();

	void go();

	void updatePreGame();
	void advance();
	void addPiece( char p, int i, chess::Position pos );
	void updateWaitGame();
	BoardXml mData;
};

};