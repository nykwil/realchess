#include "ofMain.h"
#include "ofApp.h"
#include "ofAppGlutWindow.h"
#include "Board.h"
#include "ServerGame.h"

//========================================================================
int main(int argc, char* argv[]){

	Timer::setFrameRate(30);
	string filename = "PlayerDefault.xml";
	if (argc == 2) {
		filename = string(argv[1]);
	}
	cout << "Loading Player " << filename << endl;
	Game::loadPlayer(filename);

	if (Game::mPlayerInfo.mServerMode) {
		cout << "Running in server Mode" << endl;
		chess::ServerGame* mServer = new chess::ServerGame();
		mServer->setup(11999);
		while (true) {
			mServer->go();
		}
		delete mServer;
	}
	else {
		ofAppGlutWindow window;
		ofSetupOpenGL(&window, 768, 1024, OF_WINDOW);
		ofRunApp(new testApp());
	}
}

