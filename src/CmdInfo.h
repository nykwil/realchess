#include "Board.h"

namespace chess {
	struct MoveCmdInfo : public CmdInfo {
		Maneuver mMove;

		MoveCmdInfo(int frame, const Maneuver& move) : CmdInfo(frame), mMove(move) {}

		virtual bool execute(Board& board) {
			mPiece = board.at(mMove.mFrom);
			board.removePiece(mPiece);
			mCapture = board.at(mMove.mTo);
			if (mCapture) 
				board.capturePiece(mCapture, mPiece->mColor);
			board.addPiece(mPiece, mMove.mTo);

			mPiece->moved(mMove.mTo, mFrame);
			board.calc();
			return true;
		}

		virtual void undo(Board& board) {
			board.removePiece(mPiece);
			if (mCapture) 
				board.unCapturePiece(mCapture, mPiece->mColor);
			board.addPiece(mPiece, mMove.mFrom);

			mPiece->unmoved(mMove.mFrom, mFrame);
			board.calc();
		}

		Piece* mCapture;
		Piece* mPiece;
	};

	struct AddCmdInfo : public CmdInfo {
		char mName;
		int mColor;
		Position mPos;

		AddCmdInfo(int frame, char name, int color, const Position& pos) : CmdInfo(frame), mName(name), mColor(color), mPos(pos) {}

		virtual bool execute(Board& board) {
			if (board.at(mPos) != 0)
				return false;

			mPiece = board.spawn(mName, mColor);
			board.addPiece(mPiece, mPos); 
			
			mPiece->added(mPos, mFrame);
			board.calc();
			// board.lightUpAttack(mPiece);
			return true;
		}

		virtual void undo(Board& board) {
			board.removePiece(mPiece); 
			
			mPiece->unadded(mPos, mFrame);
			board.calc();
		}

		Piece* mPiece;
	};

	struct AddOverCmdInfo : public CmdInfo {
		Piece* mPiece;
		Position mPos;

		AddOverCmdInfo(int frame, Piece* piece, const Position& pos) : CmdInfo(frame), mPiece(piece), mPos(pos) {}

		virtual bool execute(Board& board) {
			mCapture = board.at(mPos);
			if (mCapture) 
				board.capturePiece(mCapture, mPiece->mColor);
			board.addPiece(mPiece, mPos); 
		
			mPiece->added(mPos, mFrame);
			board.calc();
			// board.lightUpAttack(mPiece);
			return true;
		}

		virtual void undo(Board& board) {
			board.removePiece(mPiece);
			if (mCapture) 
				board.unCapturePiece(mCapture, mPiece->mColor);

			mPiece->unadded(mPos, mFrame);
			board.calc();
		}

		Piece* mCapture;
	};

	struct RemoveCmdInfo : public CmdInfo {
		Piece* mPiece;

		RemoveCmdInfo(int frame, Piece* piece) : CmdInfo(frame), mPiece(piece) {}

		virtual bool execute(Board& board) {
			mPos = mPiece->mPos;
			board.removePiece(mPiece);

			mPiece->bDead = true;
			board.calc();
			return true;
		}

		virtual void undo(Board& board) {
			board.addPiece(mPiece, mPos);

			mPiece->bDead = false;
			board.calc();
		}

		Position mPos;
	};

	struct StallCmdInfo : public CmdInfo {
		Piece* mPiece;

		StallCmdInfo(int frame, Piece* piece) : CmdInfo(frame), mPiece(piece) {}

		virtual bool execute(Board& board) {
			mPiece->moved(mPiece->mPos, mFrame);

			// don't need to calc everything
			mPiece->calculate(&board);

			return true;
		}

		virtual void undo(Board& board) {
			mPiece->unmoved(mPiece->mPos, mFrame);

			// don't need to calc everything
			mPiece->calculate(&board);
		}
	};

	struct EndSlideCmdInfo : public CmdInfo {
		shared_ptr<SlideInfo> mSlide;

		EndSlideCmdInfo(int frame, shared_ptr<SlideInfo>& slide) : CmdInfo(frame), mSlide(slide) {}

		virtual bool execute(Board& board) {
			board.removeSlide(mSlide);
			mCapture = board.at(mSlide->mMove.mTo);
			if (mCapture) 
				board.capturePiece(mCapture, mSlide->mPiece->mColor);
			board.addPiece(mSlide->mPiece, mSlide->mMove.mTo);

			mSlide->mPiece->moved(mSlide->mMove.mTo, mFrame);
			board.calc();
			// board.lightUpAttack(mSlide->mPiece);
			return true;
		}

		void undo(Board& board) {
			board.removePiece(mSlide->mPiece);
			if (mCapture) 
				board.unCapturePiece(mCapture, mSlide->mPiece->mColor);
			board.addSlide(mSlide);

			mSlide->mPiece->unmoved(mSlide->mMove.mFrom, mFrame);
			board.calc();
		}

		Piece* mCapture;
	};

	struct StartSlideCmdInfo : public CmdInfo {
		Maneuver mMove;

		StartSlideCmdInfo(int frame, const Maneuver& move) : CmdInfo(frame), mMove(move) {}

		virtual bool execute(Board& board) {
			mPiece = board.at(mMove.mFrom);
			float dist = ofVec2f(mMove.mFrom.x, mMove.mFrom.y).distance(ofVec2f(mMove.mTo.x, mMove.mTo.y));
			int endFrame = mFrame + (dist / mPiece->getSpeed());
			mSlide = board.addSlide(mMove, mPiece, mFrame, endFrame);
			board.removePiece(mPiece);

			board.calc();
			return true;
		}

		void undo(Board& board) {
			board.removeSlide(mSlide);
			board.addPiece(mPiece, mMove.mFrom);

			board.calc();
		}

		shared_ptr<SlideInfo> mSlide;
		Piece* mPiece;
	};
};
