#include "Position.h"
#include "ofMain.h"

using namespace chess;

Position Position::INVALID(-1, -1);

Position::Position(int x, int y) : x(x), y(y)
{
}

void Position::set( string id )
{
	x = id[0] - 'A';
	y = ofFromString<int>(id.substr(1)) - 1;
}

std::string chess::Position::getId() const
{
	string id = " " + ofToString(y + 1);
	id[0] = 'A' + x;
	return id;
}

void Position::move(int x, int y)
{
	this->x += x;
	this->y += y;
}

void Position::move(Direction d)
{
	switch(d)
	{
	case NORTH:              y -= 1; break;
	case NORTH_EAST: x += 1; y -= 1; break;
	case EAST:       x += 1;         break;
	case SOUTH_EAST: x += 1; y += 1; break;
	case SOUTH:              y += 1; break;
	case SOUTH_WEST: x -= 1; y += 1; break;
	case WEST:       x -= 1;         break;
	case NORTH_WEST: x -= 1; y -= 1; break;
	}
}

void Position::set(int x, int y)
{
	this->x = x;
	this->y = y;
}

bool Position::operator==(Position const &other) const
{
	return x == other.x && y == other.y;
}

bool Position::operator!=( Position const &other ) const
{
	return !operator==(other);
}

const Position Position::operator+( const Position& other ) const
{
	return Position(x + other.x, y + other.y);
}

