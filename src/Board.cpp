#include "Board.h"
#include "CmdInfo.h"

bool gRealMode = true;

bool chess::findIn(const PosList& posList, const Position& pos )
{
	for (PosList::const_iterator pi = posList.cbegin(); pi != posList.cend(); ++pi) {
		if (pos == *pi)
			return true;
	}
	return false;
};

using namespace chess;

Board::Board() 
{
	mLastCalc = 0; // @TODO
	mFrame = 0;
}

Board::~Board()
{
	for (auto it = mAll.begin(); it != mAll.end(); ++it) {
		delete (*it);
	}
}

void Board::setup(int players, int height, int width, const string& layout)
{
	mKings.resize(players, 0);
	mPlayerPieces.resize(players + 1);
	mCapturedPieces.resize(players + 1);
	mBoard.resize(width);

	for (int x = 0; x < mBoard.size(); ++x)
		mBoard[x].resize(height, 0); 

	mLayout = layout;
}

bool Board::hasPosition(const Position& pos) const
{
	return inBounds(pos) && mBoard[pos.x][pos.y];
}

Piece* Board::at(const Position& pos)
{
	if (inBounds(pos))
		return mBoard[pos.x][pos.y];
	else
		return 0;
}

void Board::getMoves(int color, MoveList& moves) const
{
	for (PieceList::const_iterator it = mPlayerPieces[color].cbegin(); it != mPlayerPieces[color].cend(); ++it) {
		Piece* pie = (*it);
		assert(pie->mColor == color);
		for (PosList::const_iterator pi = pie->mMoves.cbegin(); pi != pie->mMoves.cend(); ++pi) {
			moves.push_back(Maneuver(pie->mPos, *pi));
		}
		for (PosList::const_iterator pi = pie->mAttacks.cbegin(); pi != pie->mAttacks.cend(); ++pi) {
			moves.push_back(Maneuver(pie->mPos, *pi));
		}
	}
}

bool Board::isVulnerable(int color, const Position& pos) const
{
	for (int ip = 0; ip < mPlayerPieces.size(); ++ip) {
		if (ip != color) {
			for (PieceList::const_iterator it = mPlayerPieces[ip].begin(); it != mPlayerPieces[ip].end(); ++it) {
				Piece* pie = (*it);
				if (pie->isAttacking(pos)) {
					return true;
				}
			}
		}
	}

	return false;
}

bool Board::isValidMove(int color, const Maneuver& move)
{
	const Piece* piece = at(move.mFrom);
	if (piece == 0)
		return false;

	if (piece->mColor != color || color < 0)
		return false;

	if (!findIn(piece->mMoves, move.mTo) && !findIn(piece->mAttacks, move.mTo)) 
		return false;

	if (mKings[color] == 0) 
		return true;

	if (gRealMode)
		return true;
	else {
		cmdMove(move);
		bool valid = !isVulnerable(color, mKings[color]->mPos);
		undo();
		return valid;
	}
}

Status Board::getPlayerStatus(int color)
{
	if (gRealMode) {
		if (mKings[color]->bDead) 
			return Dead;
		else
			return Normal;
	}

	bool king_vulnerable = false;
	bool can_move = false;
	MoveList moves;

	getMoves(color, moves);

	if (isVulnerable(color, mKings[color]->mPos))
		king_vulnerable = true;

	for(MoveList::iterator it = moves.begin(); it != moves.end() && !can_move; ++it) {
		cmdMove(*it);
		if(!isVulnerable(color, mKings[color]->mPos))
			can_move = true;
		undo();
	}

	if(king_vulnerable && can_move)
		return InCheck;
	if(king_vulnerable && !can_move)
		return Checkmate;
	if(!king_vulnerable && !can_move)
		return Stalemate;

	return Normal;
}

void Board::print() const
{
	printf("   ___ ___ ___ ___ ___ ___ ___ ___ \n  ");

	for(int row = 7; row >= 0; row--) {
		for(int col = 0; col < 8; col++) {
			Piece* piece = mBoard[col][row];
			if (piece) {
				char repr = piece->getAscii();
				char unmoved = piece->hasMoved() ? ' ' : '.';
				char color = piece->mColor == 0 ? '`' : ' ';
				printf("|%c%c%c", unmoved, repr, color);
			}
			else {
				printf("|   ");
			}
		}
		printf("|\n%d |___|___|___|___|___|___|___|___|\n  ", row + 1);
	}
	printf("  A   B   C   D   E   F   G   H  \n\n");
}

void Board::calc() 
{
	for(int color = 0; color < mPlayerPieces.size(); ++color) {
		for (PieceList::iterator pit = mPlayerPieces[color].begin(); pit != mPlayerPieces[color].end(); ++pit) {
			(*pit)->calculate(this);
		}
	}
}

void Board::recalc(int color, const Position& from, const Position& to)
{
	for (int ip = 0; ip < mPlayerPieces.size(); ++ip) {
		for (PieceList::iterator it = mPlayerPieces[ip].begin(); it != mPlayerPieces[ip].end(); ++it) {
			Piece* pie = (*it);

			if (ip != color) {
				// go through all my enemies pieces and see if I'm in the attack list and add them to recalc
				for (PosList::iterator pit = pie->mAttacks.begin(); pit != pie->mAttacks.end(); ++pit) {
					if (*pit == from) {
						pie->calculate(this);
					}
				}

				// go through all my enemies blocks and see if I'm in the attack list and add them to recalc
				for (PosList::iterator pit = pie->mBlocks.begin(); pit != pie->mBlocks.end(); ++pit) {
					if (*pit == to) {
						pie->calculate(this);
					}
				}
			}
			else {
				// go through all my pieces and see if I'm in the block list add them to recalc
				for (PosList::iterator pit = pie->mBlocks.begin(); pit != pie->mBlocks.end(); ++pit) {
					if (*pit == from) {
						pie->calculate(this);
					}
				}
			}

			// go through all my pieces and enemy pieces and see if I'm in the move list
			for (PosList::iterator pit = pie->mMoves.begin(); pit != pie->mMoves.end(); ++pit) {
				if (*pit == to) {
					pie->calculate(this);
				}
			}
		}
	}
}

void Board::initDefaultSetup()
{
	for(int i = 0; i < 2; ++i) {
		int r = i * 7;
		addPiece(spawn("Rook", i), Position(0, r));
		addPiece(spawn("Knight", i), Position(1, r));
		addPiece(spawn("Bishop", i), Position(2, r));
		addPiece(spawn("King", i), Position(3, r));
		addPiece(spawn("Queen", i), Position(4, r));
		addPiece(spawn("Bishop", i), Position(5, r));
		addPiece(spawn("Knight", i), Position(6, r));
		addPiece(spawn("Rook", i), Position(7, r));

		r = 1 + i * 5;
		for (int j = 0; j < 8; ++j) {
			addPiece(spawn("Pawn", i), Position(j, r));
		}
	}
	calc();
}

int Board::evaluateBoard(int color)
{
	int sum = 0;
	for (int ip = 0; ip < mPlayerPieces.size(); ++ip) {
		for (PieceList::iterator it = mPlayerPieces[ip].begin(); it != mPlayerPieces[ip].end(); ++it) {
			Piece* piece = (*it);
			if (piece->mColor == color)
				sum += piece->getValue();
			else
				sum -= piece->getValue();
		}
	}
	return sum;
}

void Board::draw()
{
	updateRenderer();

	Position pos;
	int color = 0;
	for (pos.x = 0; pos.x < mBoard.size(); ++pos.x) {
		for (pos.y = 0; pos.y < mBoard[pos.x].size(); ++pos.y) {
			if (getLayout(pos)) {
				ofSetColor(COLOR_BOARD[color]);
				drawRect(pos);
			}
			else {
				ofSetColor(ofColor::green);
				drawRect(pos);
			}
			color = (color + 1) % 2;
		}
		color = (color + 1) % 2;
	}

	// the lights
	for (list<LightInfo>::iterator it = mLightUps.begin(); it != mLightUps.end(); ++it) {
		LightInfo& li = *it;

		if (li.mPos == li.mStartPos) {
			ofSetColor(li.mColor);
			drawRect(li.mPos);
		}
		else {
			int diff = max(abs(li.mStartPos.x - li.mPos.x), abs(li.mStartPos.y - li.mPos.y));
			int frm = (mFrame - li.mStartFrame) / 4;
			if (frm - 2 == diff) {
				ofSetColor(li.mColor.r, li.mColor.g, li.mColor.b, li.mColor.a * 0.4f);
				drawRect(li.mPos);
			}
			else if (frm - 1 == diff) {
				ofSetColor(li.mColor.r, li.mColor.g, li.mColor.b, li.mColor.a * 0.75f);
				drawRect(li.mPos);
			}
			else if (frm == diff) {
				ofSetColor(li.mColor.r, li.mColor.g, li.mColor.b, li.mColor.a);
				drawRect(li.mPos);
			}
			else if (frm + 1 == diff) {
				ofSetColor(li.mColor.r, li.mColor.g, li.mColor.b, li.mColor.a * 0.75f);
				drawRect(li.mPos);
			}
			else if (frm + 2 == diff) {
				ofSetColor(li.mColor.r, li.mColor.g, li.mColor.b, li.mColor.a * 0.4f);
				drawRect(li.mPos);
			}
		}
	}

	for (int ip = 0; ip < mPlayerPieces.size(); ++ip) {
		for (PieceList::iterator it = mPlayerPieces[ip].begin(); it != mPlayerPieces[ip].end(); ++it) {
			Piece* pie = (*it);
			if (pie->isInCooldown(getFrame())) {
				pie->drawRing();
			}
			pie->draw();
		}
	}
	for (auto it = mKeepCmds.begin(); it != mKeepCmds.end(); ++it) {
		shared_ptr<AddOverCmdInfo> cmd = *it;

		if (cmd->mFrame - 10 < mFrame && mFrame < cmd->mFrame) {
			ofPushMatrix();
			ofTranslate(Board::b2s(ofVec2f(cmd->mPos.x + 0.5f, cmd->mPos.y + 0.5f))); // make t

			ofSetColor(Piece::OUT_RING_COLOR);
			cmd->mPiece->mOutMesh.draw();
			ofSetColor(Piece::IN_RING_COLOR);
			cmd->mPiece->mInMesh.draw();

			cmd->mPiece->drawPiece(255);
			ofPopMatrix();
		}
	}

	for (auto it = mSlides.begin(); it != mSlides.end(); ++it) {
		shared_ptr<SlideInfo> slide = *it;
		ofVec2f frm = Board::b2s(slide->mMove.mFrom);
		ofVec2f to = Board::b2s(slide->mMove.mTo);
		ofVec2f p = frm.getInterpolated(to, ofMap((float)getFrame(), (float)slide->mStart, (float)slide->mEnd, 0, 1, true));
		ofPushMatrix();
		ofTranslate(p.x + Board::gBoardScale * 0.5, p.y + Board::gBoardScale * 0.5); // make t
		slide->mPiece->drawPiece(200);
		ofPopMatrix();
	}
}

void Board::addPiece(Piece* piece, Position pos)
{
	if (piece->getAscii() == 'k')
		mKings[piece->mColor] = piece;

	piece->mPos = pos;
	mBoard[pos.x][pos.y] = piece;
	mPlayerPieces[piece->mColor].push_back(piece);
	piece->bDead = false;
}

void Board::removePiece(Piece* piece)
{
	if (at(piece->mPos) == piece) {
		mBoard[piece->mPos.x][piece->mPos.y] = 0;
	}
	for (PieceList::iterator it = mPlayerPieces[piece->mColor].begin(); it != mPlayerPieces[piece->mColor].end(); ++it) {
		if (*it == piece) {
			mPlayerPieces[piece->mColor].erase(it);
			break;
		}
	}
}

Piece* Board::spawn(const string& name, int color)
{
	 Piece* piece = 0;
	 if (name.compare("Rook") == 0 || name.compare("r") == 0)
		 piece = new Rook(color);
	 else if (name.compare("Knight") == 0 || name.compare("n") == 0)
		 piece = new Knight(color);
	 else if (name.compare("Bishop") == 0 || name.compare("b") == 0)
		 piece = new Bishop(color);
	 else if (name.compare("Queen") == 0 || name.compare("q") == 0)
		 piece = new Queen(color);
	 else if (name.compare("King") == 0 || name.compare("k") == 0)
		 piece = new King(color);
	 else if (name.compare("Pawn") == 0 || name.compare("p") == 0)
		 piece = new Pawn(color);
	 else if (name.compare("Coin") == 0 || name.compare("c") == 0)
		 piece = new Coin(color);
	
	mAll.push_back(piece);
		
	piece->setup("simple");

	return piece;
}

Piece* Board::spawn(char name, int color)
{
	return spawn(ofToString(name), color);
}

struct TimestampSort
{
	bool operator()(const shared_ptr<CmdInfo>& t1, const shared_ptr<CmdInfo>& t2) const 
	{ 
		return t2->mFrame < t1->mFrame;
	}
};

void Board::advance()
{
	for (int i = 0; i < mPlayerPieces.size(); ++i) {
		for (PieceList::iterator it = mPlayerPieces[i].begin(); it != mPlayerPieces[i].end(); ++it) {
			Piece* pie = *it;
			if (!pie->isInCooldown(getFrame())) {
				Position force;
				if (pie->mBehavior && pie->mBehavior->calcForce(force, pie, this)) {
					if (force != pie->mPos) {
						mQueueCmds.push_back(make_shared<StartSlideCmdInfo>(getFrame(), Maneuver(pie->mPos, force)));
					}
					else {
						mQueueCmds.push_back(make_shared<StallCmdInfo>(getFrame(), pie));
					}
				}
			}
		}
	}

	for (list<LightInfo>::iterator it = mLightUps.begin(); it != mLightUps.end(); ) {
		--it->mFrames;
		if (it->mFrames < 0) 
			it = mLightUps.erase(it);
		else
			++it;
	}

	for (auto it = mSlides.begin(); it != mSlides.end(); ++it) {
		shared_ptr<SlideInfo> slide = *it;
		if (slide->mEnd == getFrame()) {
			mQueueCmds.push_back(make_shared<EndSlideCmdInfo>(getFrame(), slide));
		}
	}

	// only sort if things have changed
	// assume sorted so you don't have to go through all the queued commands
	mQueueCmds.sort(TimestampSort());
	for (auto it = mQueueCmds.begin(); it != mQueueCmds.end(); ) {
		if ((*it)->mFrame <= getFrame()) {
			execute(*it);
			it = mQueueCmds.erase(it);
		}
		else {
			++it;
		}
	}

	for (auto it = mKeepCmds.begin(); it != mKeepCmds.end(); ++it) {
		shared_ptr<AddOverCmdInfo> cmd = *it;

		if (cmd->mFrame == getFrame()) {
			execute(*it);
		}
	}

	mFrame++;
}

int Board::getWidth() const
{
	return mBoard.size();
}

int Board::getHeight() const // @look using this doesn't support irregular board sizes
{
	return mBoard[0].size();
}

void Board::lightUp(const PosList& poslist, int frames, const ofColor& color)
{
	for (PosList::const_iterator it = poslist.cbegin(); it != poslist.cend(); ++it) {
		mLightUps.push_back(LightInfo(*it, color, frames));
	}
}

void Board::slightUp(const PosList& poslist, const Position& startPos, int frames, int startFrame, const ofColor& color)
{
	for (PosList::const_iterator it = poslist.cbegin(); it != poslist.cend(); ++it) {
		mLightUps.push_back(LightInfo(*it, color, frames, startPos, startFrame));
	}
}

void Board::lightUpAttack(Piece* piece)
{
	slightUp(piece->mMoves, piece->mPos, 60, mFrame, COLOR_ATTACK);
	slightUp(piece->mAttacks, piece->mPos, 60, mFrame, COLOR_ATTACK2);
}

void Board::lightUpValid(Piece* piece)
{
	lightUp(piece->mMoves, 1, COLOR_VALID);
	lightUp(piece->mAttacks, 1, COLOR_ATTACK2);
}

void Board::undoFrame()
{
	--mFrame;
	while (!mUndoCmds.empty() && mUndoCmds.back()->mFrame > mFrame) {
		undo();
	}
}

void Board::reset()
{
	mKings.clear();
	while (!mUndoCmds.empty()) {
		undo();
	}
	mLightUps.clear();

	for (int ip = 0; ip < mPlayerPieces.size(); ++ip) {
		for (PieceList::const_iterator it = mPlayerPieces[ip].begin(); it != mPlayerPieces[ip].end();) {
			Piece* piece = (*it);
			piece->bDead = true;
			if (at(piece->mPos) == piece) {
				mBoard[piece->mPos.x][piece->mPos.y] = 0;
			}
			it = mPlayerPieces[ip].erase(it);
		}
	}
	mFrame = 0;
}

bool Board::cmdAdd(char pieceName, int color, const Position& pos)
{
	return execute(make_shared<AddCmdInfo>(getFrame(), pieceName, color, pos));
}

bool Board::cmdMove(const Maneuver& move)
{
	return execute(make_shared<MoveCmdInfo>(getFrame(), move));
}

bool Board::cmdSlide(const Maneuver& move)
{
	return execute(make_shared<StartSlideCmdInfo>(getFrame(), move));
}

shared_ptr<SlideInfo> Board::addSlide(const Maneuver& mve, Piece* piece, int startFrame, int endFrame)
{
	shared_ptr<SlideInfo>& info = make_shared<SlideInfo>(mve, piece, startFrame, endFrame);
	mSlides.push_back(info);
	return info;
}

void Board::addSlide(shared_ptr<SlideInfo>& info)
{
	mSlides.push_back(info);
}

void Board::removeSlide(shared_ptr<SlideInfo>& slide)
{
	for (auto it = mSlides.begin(); it != mSlides.end(); ++it) {
		if (slide->mPiece == (*it)->mPiece) {
			mSlides.erase(it);
			return;
		}
	}
	assert(0);
}

bool Board::execute(shared_ptr<CmdInfo> info)
{
	if (info->execute(*this)) {
		mUndoCmds.push_back(info);
		return true;
	}
	return false;
}

void Board::undo()
{
	shared_ptr<CmdInfo>& cmd = mUndoCmds.back();
	cmd->undo(*this);
	mUndoCmds.pop_back();
}

bool Board::inBounds(const Position& pos) const
{
	if (pos.x < 0 || pos.x >= mBoard.size() || pos.y < 0 || pos.y >= mBoard[pos.x].size())
		return false;

	if (!getLayout(pos))
		return false;

	return true;
}

bool Board::inside(const ofVec2f& pos) const
{
	ofVec2f sp = s2b(pos);
	return sp.x >= 0 && sp.x < getWidth() && sp.y >= 0 && sp.y < getHeight();
}

Position Board::modPos(Position pos) const
{
	while (pos.x < 0)
		pos.x += getWidth();
	while (pos.x > getWidth())
		pos.x -= getWidth();
	while (pos.y < 0)
		pos.y += getHeight();
	while (pos.y > getHeight())
		pos.y -= getHeight();

	return pos;
}

//@todo make better
Position Board::pickFair()
{
	ofVec2f sp;
	ofVec2f ep;
	float weight = (float)(rand() % 99) / 99.f; // 0 == player 1, 1 == player 2

	if (mKings[0]) {
		sp.set(mKings[0]->mPos.x, mKings[0]->mPos.y);
	}
	if (mKings[1]) {
		ep.set(mKings[1]->mPos.x, mKings[1]->mPos.y);
		weight = ofMap(getScore(1) - getScore(0), -10, 10, 0, 1, true);
	}

	ofVec2f dir = ep - sp;

	dir *= weight;
	ofVec2f perp = dir.getPerpendicular();
	Position fp;
	for (int i = 0; i < 10; ++i) {
		ofVec2f p = sp + (dir * weight * (float)(rand() % 3 - 1)) + (perp * (float)(rand() % 9 - 4));
		Position pp(p.x, p.y);
		if (inBounds(pp) && at(pp) == 0) {
			fp = pp;
			break;
		}
	}

	return fp;
}

bool Board::getLayout(const Position& pos) const
{
	char c = ' ';
	int i = pos.x + pos.y * getWidth();
	if (mLayout.size() > i) 
		c = mLayout[i];

	return c != 'X';
}

void Board::setLayout(const Position& pos, bool valid)
{
	int i = pos.x + pos.y * getWidth();
	if (mLayout.size() <= i) {
		mLayout.resize(i + 1);
	}
	mLayout[i] = valid ? ' ' : 'X';
}

void Board::calcNearest(const Position& from)
{
	static vector<Position> dirs;
	if (dirs.empty()) {
		dirs.push_back(Position( 1,  0));
		dirs.push_back(Position( 1,  1));
		dirs.push_back(Position( 0,  1));
		dirs.push_back(Position(-1,  1));
		dirs.push_back(Position(-1,  0));
		dirs.push_back(Position(-1, -1));
		dirs.push_back(Position( 0, -1));
		dirs.push_back(Position( 1, -1));
	}

	mNearestGrid.clear();
	mNearestGrid.resize(getWidth());
	for (int w = 0; w < getWidth(); ++w) {
		mNearestGrid[w].resize(getHeight(), -1);
	}

	vector<Position> positions1;
	vector<Position> positions2;

	int distance = 0;
	positions1.push_back(from);
	mNearestGrid[from.x][from.y] = distance;
	++distance;

	bool first = true;

	while (first ? !positions1.empty() : !positions2.empty()) {
		// visit neig
		vector<Position>& psLast = first ? positions1 : positions2;
		vector<Position>& psNext = first ? positions2 : positions1;
		psNext.clear();
		for (auto it = psLast.begin(); it != psLast.end(); ++it) {
			const Position& p = *it;
			for (auto dit = dirs.begin(); dit != dirs.end(); ++dit) {
				Position np = p + (*dit);
				if (inBounds(np) && mNearestGrid[np.x][np.y] < 0) { // and valid
					mNearestGrid[np.x][np.y] = distance;
					psNext.push_back(np);
				}
			}
		}

		printf("\n");
		for (int w = 0; w < getWidth(); ++w) {
			for (int h = 0; h < getHeight(); ++h) {
				printf(" %c ", '0' + mNearestGrid[w][h]);
			}
			printf("\n");
		}

		first = !first; // swap old poses and new poses
		++distance;
	}
}

int Board::getNearestScore(const Position& from, const Position& to)
{
	if (to != mNearestTo) {
		calcNearest(to);
	}
	return mNearestGrid[from.x][from.y];
}

void chess::Board::updateRenderer()
{
	for (auto it = mKeepCmds.begin(); it != mKeepCmds.end(); ++it) {
		shared_ptr<AddOverCmdInfo> cmd = *it;

		if (cmd->mFrame > getFrame()) {
			int d = cmd->mFrame - getFrame();
			if (d <= 10) {
				buildRingMeshRad(cmd->mPiece->mInMesh, Board::gBoardScale * Piece::COOL_IN_RING, Board::gBoardScale * Piece::COOL_OUT_RING, 0, (float)d / 10.f * TWO_PI);
			}
		}
	}

	for (int ip = 0; ip < mPlayerPieces.size(); ++ip) {
		for (PieceList::iterator it = mPlayerPieces[ip].begin(); it != mPlayerPieces[ip].end(); ++it) {
			Piece* pie = (*it);
			if (pie->isInCooldown(getFrame())) {
				float f = (float)(pie->mCooldown - getFrame()) / pie->getCooldownTime();
				buildRingMeshRad(pie->mInMesh, Board::gBoardScale * Piece::COOL_IN_RING, Board::gBoardScale * Piece::COOL_OUT_RING, 0, f * TWO_PI);
			}
		}
	}
}

