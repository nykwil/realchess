#include "ofApp.h"
#include "ServerGame.h"

void testApp::setup() {
	ofEnableAlphaBlending();
	ofSetFrameRate(30);

	mTimeLastFrame = 0;
	mTimeAcumulator = 0;

	RealManager::instance.setup();
}

void testApp::update() {
	// Calculate time since last frame and remember current time for next frame
	unsigned long lTimeCurrentFrame = ofGetElapsedTimeMillis();

	if (RealManager::instance.bDebug) {
		if (lTimeCurrentFrame - mTimeLastFrame > Timer::getDesiredFrameTime()) {
			RealManager::instance.update(Timer::getDesiredFrameTime() / 1000.f);
			mTimeLastFrame = lTimeCurrentFrame;
		}
	}
	else {
		unsigned long lTimeSinceLastFrame = lTimeCurrentFrame - mTimeLastFrame;
		mTimeLastFrame = lTimeCurrentFrame;
		mTimeAcumulator += lTimeSinceLastFrame;

		while (mTimeAcumulator >= Timer::getDesiredFrameTime()) 
		{
			RealManager::instance.update(Timer::getDesiredFrameTime() / 1000.f);
			mTimeAcumulator -= Timer::getDesiredFrameTime();
		}
	}
}

void testApp::draw() {
	RealManager::instance.draw();
}

void testApp::keyPressed(int key) {
	if (key == 32) 	{
		RealManager::instance.toggleSettings();
	}
}

void testApp::keyReleased(int key) {
}

void testApp::mouseMoved(int x, int y ) {
	RealManager::instance.mouseMove(x, y);
}

void testApp::mouseDragged(int x, int y, int button){
	RealManager::instance.touchMoved(x, y, button);
}

void testApp::mousePressed(int x, int y, int button){
	RealManager::instance.touchDown(x, y, button);
}

void testApp::mouseReleased(int x, int y, int button){
	RealManager::instance.touchUp(x, y, button);
}

void testApp::windowResized(int w, int h){

}

void testApp::gotMessage(ofMessage msg){

}

void testApp::dragEvent(ofDragInfo dragInfo){ 

}

