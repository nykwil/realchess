#pragma once

#include "pch.h"
#include "ofMain.h"
#include "Position.h"
#include "PropertyContainer.h"

namespace StringConverter {
	void fromString(chess::PosList& f, const string& str);
	string toString(const chess::PosList& vec);
	void fromString(chess::Position& f, const string& str);
	string toString(const chess::Position& vec);
};

namespace chess
{
	class Board;

	const int WIN_VALUE     = 50000;	// win the game

	class Bee : public PropertyObject{
	public:
		virtual bool calcForce(Position& force, Piece* piece, Board* board) = 0;
	};

	class AttackBee : public Bee {
	public:
		virtual bool calcForce(Position& force, Piece* piece, Board* board);
	};

	class HunterBee : public AttackBee {
	public:
		virtual bool calcForce(Position& force, Piece* piece, Board* board);
	};

	class SeekerBee : public AttackBee {
	public:
		virtual string getType() { return "SeekerBee"; }
		SeekerBee() {
			newProperty("Positions", mPositions);
//			newProperty("CooldownMult", mCooldownMult, 1.f);
			newProperty("Loop", mLoop, true);
			newProperty("LoopPoint", mLoopPoint, 0);
		}

		PosList mPositions;
		bool mLoop;
		int mLoopPoint;

		virtual void setup(const Position& offset);

		Position mOffset;
		virtual bool calcForce(Position& force, Piece* piece, Board* board);
	};

	class Piece {
	public:
		Piece(int color);
		virtual ~Piece() { if (mBehavior) delete mBehavior; };

		int mColor;
		bool bDead;
		bool bSelected;
		bool bDirty;

		ofImage mImage;

		Position mPos;
		int mMoveCount;

		// Don't make moves without caching this
		PosList mMoves;
		PosList mAttacks;
		PosList mBlocks;

		int mCooldown;
		float mCooldownMult;

		ofVboMesh mOutMesh;
		ofVboMesh mInMesh;

		static float COOL_OUT_RING;
		static float COOL_IN_RING;
		static ofColor IN_RING_COLOR;
		static ofColor OUT_RING_COLOR;
		static float SPEED;

	public:
		void setup(const string& setName);
		virtual void drawPiece(int alpha);
		virtual void drawRing();
		virtual void draw();
		virtual char getAscii() const { return ' '; }
		virtual int getValue() const { return 0; }
		virtual int getCooldownTime() const { return 1; }
		virtual float getSpeed() { return SPEED; }

		bool isInCooldown(int frame) { return mCooldown > frame; }

		virtual void calculate(Board* board) {}

		bool isAttacking( const Position& pos ) const;
		bool hasMoved() const { return mMoveCount == 0; }

		void added(const Position& pos, int frame);
		void unadded(const Position& pos, int frame);
		void moved(const Position& pos, int frame);
		void unmoved(const Position& pos, int frame);

		Position getNearestPos(const Position& pos, float random = 0.5f) { return getNearestPos(ofVec2f(pos.x, pos.y), mPos, random); }
		Position getNearestPos(const ofVec2f& pos, float random = 0.5f)  { return getNearestPos(pos, mPos, random); }
		Position getNearestPos(const Position& pos, const Position& startPos, float random = 0.5f) { return getNearestPos(ofVec2f(pos.x, pos.y), startPos, random); }
		Position getNearestPos(const ofVec2f& pos, const Position& startPos, float random = 0.5f);

		Bee* mBehavior;

	protected:
		bool addPosition(Board* board, const Position& pos);
		void shootPath(Board* board, Position pos, Direction d);
	};

	class Coin : public Piece {
	public:
		Coin(int color) : Piece(color) {};
		virtual void calculate(Board* board) {}
		virtual char getAscii() const { return 'c'; }
		virtual int getValue() const { return VALUE; }
		virtual int getCooldownTime() const { return 0; }
		static int VALUE;
	};
	class Pawn : public Piece {
	public:
		Pawn(int color) : Piece(color) {};
		virtual void calculate(Board* board);
		virtual char getAscii() const { return 'p'; }
		virtual int getValue() const { return VALUE; }
		virtual int getCooldownTime() const { return COOLDOWN * mCooldownMult; }
		static int VALUE;
		static int COOLDOWN;
	};
	class Knight : public Piece {
	public:
		Knight(int color) : Piece(color) {};
		virtual void calculate(Board* board);
		virtual char getAscii() const { return 'n'; }
		virtual int getValue() const { return VALUE; }
		virtual int getCooldownTime() const { return COOLDOWN * mCooldownMult; }
		static int VALUE;
		static int COOLDOWN;
	};
	class Bishop : public Piece {
	public:
		Bishop(int color) : Piece(color) {};
		virtual void calculate(Board* board);
		virtual char getAscii() const { return 'b'; }
		virtual int getValue() const { return VALUE; }
		virtual int getCooldownTime() const { return COOLDOWN * mCooldownMult; }
		static int VALUE;
		static int COOLDOWN;
	};
	class Rook : public Piece {
	public:
		Rook(int color) : Piece(color) {};
		virtual void calculate(Board* board);
		virtual char getAscii() const { return 'r'; }
		virtual int getValue() const { return VALUE; }
		virtual int getCooldownTime() const { return COOLDOWN * mCooldownMult; }
		static int VALUE;
		static int COOLDOWN;
	};
	class Queen : public Piece {
	public:
		Queen(int color) : Piece(color) {};
		virtual void calculate(Board* board);
		virtual char getAscii() const { return 'q'; }
		virtual int getValue() const { return VALUE; }
		virtual int getCooldownTime() const { return COOLDOWN * mCooldownMult; }
		static int VALUE;
		static int COOLDOWN;
	};

	class King : public Piece {
	public:
		King(int color) : Piece(color) {};
		virtual void calculate(Board* board);
		virtual char getAscii() const { return 'k'; }
		virtual int getValue() const { return VALUE; }
		virtual int getCooldownTime() const { return COOLDOWN * mCooldownMult; }
		static int VALUE;
		static int COOLDOWN;
	};

	typedef std::vector<Piece*> PieceArr;
	typedef std::list<Piece*> PieceList;
};
