#include "ServerGame.h"
#include "Timer.h"
#include "Loader.h"

#define LATENCY_ADD 2

using namespace chess;

TFactory<BoardXml> factBoard("Board");
TFactory<PieceXml> factPiece("Piece");

int getTimestamp(char str[])
{
	int i = *(int*)&str[1];
	return i;
}

struct TimestampSort
{
	bool operator()(const TimeMsg& t1, const TimeMsg& t2) const 
	{ 
		return t2.mTimeStamp < t1.mTimeStamp;
	}
};

void ServerGame::setup( int port )
{
	mBoard.setup(2, 8, 8, "");
	mTcp.setup(port);
	mTimeLastFrame = 0;
	mTimeAcumulator = 0;

	mClientsReady = 0;
	setState(SS_PRE);

	Loader::load("data/basic_setup.xml", &mData);
}

void ServerGame::updatePreGame()
{
	// Get the client moves
	for(int cid = 0; cid < mTcp.getLastID(); ++cid){
		if (mTcp.isClientConnected(cid)) {
			int rcv = mTcp.receiveRawBytes(cid, buffer, TCP_MAX_MSG_SIZE);
			for (int bi = 0; bi < rcv; bi += MAX_MSG) {
				if (Game::isDebugMode()) cout << "Server In:" << buffer[bi] << endl;
				if (buffer[bi] == MSG_CONNECTED) {
					for (map<int, string>::iterator it = mClientNames.begin(); it != mClientNames.end(); ++it) {
						if (Game::isDebugMode()) cout << "Server to " << cid << " name:" << it->second << endl;
						ConnectedMsg ms(it->second.c_str());
						mTcp.sendRawBytes(cid, (const char*)&ms, MAX_MSG);
					}

					ConnectedMsg& msg = (ConnectedMsg&)buffer[bi];
					mTcp.sendRawBytesToAll((const char*)&msg, MAX_MSG);
					mClientNames[cid] = msg.mName;
					if (Game::isDebugMode()) cout << "Server Out:" << (const char*)&msg << endl;
				}
				else if (buffer[bi] == MSG_READY) {
					ReadyMsg& msg = (ReadyMsg&)buffer[bi];

					// resend
					mTcp.sendRawBytesToAll((const char*)&msg, MAX_MSG);
					if (Game::isDebugMode()) cout << "Server Out:" << (const char*)&msg << endl;
					if (msg.bReady)
						++mClientsReady;
					else
						--mClientsReady;
				}
			}
		}
	}

	// start game
	if (mClientsReady > 0 && mClientsReady == mTcp.getNumClients()) {
		setState(SS_WAIT);
	}
}

void ServerGame::advance()
{
	// Get the client moves
	for(int cid = 0; cid < mTcp.getLastID(); ++cid){
		if (mTcp.isClientConnected(cid)) {
			int rcv = mTcp.receiveRawBytes(cid, buffer, TCP_MAX_MSG_SIZE);
			for (int bi = 0; bi < rcv; bi += MAX_MSG) {
				if (Game::isDebugMode()) cout << "Server In:" << buffer[bi] << endl;
				TimeMsg& msg = (TimeMsg&)buffer[bi];
				mMsgs.push_back(msg);
			}
		}
	}

	mMsgs.sort(TimestampSort());

	// process client moves and send
	for (list<TimeMsg>::iterator it = mMsgs.begin(); it != mMsgs.end(); ) {
		if (it->mTimeStamp + LATENCY_ADD <= mBoard.getFrame()) {
			if (it->mType == MSG_MOVE) {
				MoveMsg& inmsg = (MoveMsg&)(*it);

				if (mBoard.isValidMove(inmsg.mColor, inmsg.getMove())) {
					mBoard.cmdSlide(inmsg.getMove());
					// Status status = mBoard->getPlayerStatus(mTurn);

					MoveMsg msg(inmsg.mColor, mBoard.getFrame(), inmsg.getMove());
					mTcp.sendRawBytesToAll((const char*)&msg, MAX_MSG);
					if (Game::isDebugMode()) cout << "Server Out:" << (const char*)&msg << endl;
				}
				else {
					cout << "Server: invalid move" << endl;
				}

			}
			else if (it->mType == MSG_ADDPIECE) {
				AddPieceMsg& inmsg = (AddPieceMsg&)(*it);

				if (mBoard.at(inmsg.getPosition()) == 0) {
					mBoard.cmdAdd(inmsg.getPiece(), inmsg.mColor, inmsg.getPosition());

					AddPieceMsg msg(inmsg.mColor, mBoard.getFrame(), inmsg.getPiece(), inmsg.getPosition());
					mTcp.sendRawBytesToAll((const char*)&msg, MAX_MSG);
					if (Game::isDebugMode()) cout << "Server Out:" << (const char*)&msg << endl;
				}
				else {
					ofLogError() << "Server: invalid add" << endl;
				}
			}
			else {
				ofLogError() << "Invalid Message" << endl;
			}
			it = mMsgs.erase(it);
		}
		else {
			++it;
		}
	}

	mBoard.advance();

	for (int i = 0; i < mBoard.getPlayerCount(); ++i) {
		if (mBoard.getPlayerStatus(i) == Dead) {
			EndGameMsg msg(i);
			mTcp.sendRawBytesToAll((const char*)&msg, MAX_MSG);
			if (Game::isDebugMode()) cout << "Out:" << (const char*)&msg << endl;
			setState(SS_PRE);
		}
	}
}

void ServerGame::setState( ServerState state )
{
	if (mState == SS_GAME && state == SS_PRE) {
		// @todo do end game stuff
	}
	mState = state;
}

void ServerGame::threadedFunction()
{
	while (isThreadRunning()) {
		go();
	}
}

void ServerGame::addPiece( char p, int i, Position pos )
{
	AddPieceMsg msg(i, 0, p, pos);
	mTcp.sendRawBytesToAll((const char*)&msg, MAX_MSG);
	if (Game::isDebugMode()) cout << "Out:" << (const char*)&msg << endl;
	mBoard.cmdAdd(p, i, pos);
}

void ServerGame::updateWaitGame()
{
	assert(mState == 1);

	for (vector<PieceXml*>::iterator it = mData.mPieces.begin(); it != mData.mPieces.end(); ++it) {
		addPiece((*it)->mPieceName, (*it)->mColor, (*it)->mPos);
	}

	int color = 0;

	for(int cid = 0; cid < mTcp.getLastID(); ++cid){
		if (mTcp.isClientConnected(cid)) {
			StartGameMsg msg(color++);
			mTcp.sendRawBytes(cid, (const char*)&msg, MAX_MSG);
			if (Game::isDebugMode()) cout << "Server Out:" << (const char*)&msg << endl;
		}
	}


	setState(SS_GAME);
}

void ServerGame::go()
{
	unsigned long lTimeCurrentFrame = ofGetElapsedTimeMillis();

	unsigned long lTimeSinceLastFrame = lTimeCurrentFrame - mTimeLastFrame;
	mTimeLastFrame = lTimeCurrentFrame;
	mTimeAcumulator += lTimeSinceLastFrame;

	while (mTimeAcumulator >= Timer::getDesiredFrameTime()) 
	{
		if (mState == 0) {
			updatePreGame();
		}
		else if (mState == 1) {
			updateWaitGame();
		}
		else  {
			advance();
		}
		mTimeAcumulator -= Timer::getDesiredFrameTime();
	}
}
