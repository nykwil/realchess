#include "SingleplayerGame.h"
#include "Loader.h"

using namespace chess;

EditorGameState::EditorGameState()
{
	gui = make_shared<GuiCanvas>(0, 0, ofGetWidth(), ofGetHeight());
	gui->addWidget(shared_ptr<GuiButton>(new GuiButton(0, 0, 96, 48, "<<", this, &EditorGameState::onPrevStep)));
	gui->addWidget(shared_ptr<GuiButton>(new GuiButton(96, 0, 96, 48, "<-", this, &EditorGameState::onPrevFrame)));
	gui->addWidget(shared_ptr<GuiButton>(new GuiButton(96 * 3, 0, 96, 48, "Save", this, &EditorGameState::onSave)));
	gui->addWidget(shared_ptr<GuiButton>(new GuiButton(96 * 4, 0, 96, 48, "Quit", this, &EditorGameState::onQuit)));
	gui->addWidget(shared_ptr<GuiButton>(new GuiButton(96 * 5, 0, 96, 48, "Edit", this, &EditorGameState::onEditBoard)));
	gui->addWidget(shared_ptr<GuiButton>(new GuiButton(ofGetWidth() - 192, 0, 96, 48, "->", this, &EditorGameState::onNextFrame)));
	gui->addWidget(shared_ptr<GuiButton>(new GuiButton(ofGetWidth() - 96, 0, 96, 48, ">>", this, &EditorGameState::onNextStep)));

	bEditBoard = false;
}

void EditorGameState::enter()
{
	load();
	if (mData.mWaves.empty()) {
		mData.mWaves.push_back(new WaveXml());
	}
	else {
		mData.mWaves[0]->setup(&mBoard, 1);
	}

	mSelected = 0;
	mBankSelected = 0;
	mBankBounds.set(Board::b2s(0, mBoard.getHeight()), Board::b2s(mBoard.getWidth(), mBoard.getHeight() + 1));

	string s = "qrbp"; // @todo all possible 
	for (int i = 0; i < s.size(); ++i) {
		Piece* pie = mBoard.spawn(s[i], 1); 
		mBankPieces.push_back(pie);
	}

	addChild(gui.get());
}

void EditorGameState::exit()
{
	removeChild(gui.get());
}

void EditorGameState::onEditBoard()
{
	bEditBoard = !bEditBoard;
}

void EditorGameState::onQuit()
{
	RealManager::instance.onQuitGame();
}

void EditorGameState::load()
{
	if (!Loader::load(Game::mPlayerInfo.mLevelFile, &mData)) {
		assert(0);
	}
	mBoard.setup(2, 8, 8, mData.mLayout);
}

void EditorGameState::save()
{
	mData.mLayout = mBoard.getFullLayout();
	Loader::save(Game::mPlayerInfo.mLevelFile, &mData);
}

void EditorGameState::setFrame(int frame)
{
	while (mBoard.getFrame() > frame) {
		mBoard.undoFrame();
	}

	while (mBoard.getFrame() < frame) {
		mBoard.advance();
	}
}

void EditorGameState::update(float dt)
{
	if (mSelected) {
		if (mSelected->mPiece->bDead) {
			mSelected->mPiece->bSelected = false;
			mSelected = 0;
		}
	}
}

// selected
//  touch empty spot set behavior move
bool EditorGameState::touchDown(float x, float y, int touchId)
{
	ofVec2f point(x,y);

	if (mBoard.inside(point)) {
		if (bEditBoard) {
			Position pos = Board::s2b(x, y);
			mBoard.setLayout(pos, !mBoard.getLayout(pos));
		}
		return true;
	}

	if (mBankBounds.inside(point)) {
		ofVec2f pos = Board::s2b(point);
		Piece* piece = mBankPieces[(int)(pos.x + 0.001)];

		if(mBankSelected) {
			if (piece == mBankSelected) {
				mBankSelected->bSelected = false;
				mBankSelected = 0;
			}
		}
		else {
			if (piece) {
				mBankSelected = piece;
				mBankSelected->bSelected = true;
			}
		}
		return true;
	}
	return TouchState::touchDown(x, y, touchId);
}

bool EditorGameState::touchMoved(float x, float y, int touchId)
{
	if (mBoard.inside(ofVec2f(x,y))) {
		return true;
	}
	if (mBankSelected) {
		return true;
	}

	return TouchState::touchMoved(x, y, touchId);
}

bool EditorGameState::touchUp(float x, float y, int touchId)
{
	ofVec2f point(x,y);
	if (mBoard.inside(point)) {
		ofVec2f p = Board::s2b(point);
		Position pos(p.x, p.y);
		Piece* piece = mBoard.at(pos);
		if (mSelected) {
			mSelected->editPos(pos);
			mSelected->mPiece->bSelected = false;
			mSelected = 0;
		}
		else {
			if (piece) {
				for (vector<AIPieceXml*>::iterator it = mData.mWaves[0]->mBehaviors.begin(); it != mData.mWaves[0]->mBehaviors.end(); ++it) {
					if ((*it)->mPiece == piece) {
						mSelected = *it;
						mSelected->mPiece->bSelected = true;
						break;
					}
				}
			}
		}
		if (mBankSelected) {
			Position p = Board::s2b(x, y);
			Piece* piece = mBoard.at(p);
			if (mBoard.inBounds(p) && piece == 0) {
				mData.mWaves[0]->mBehaviors.push_back(createPiece(p));
				mBankSelected->bSelected = false;
				mBankSelected = 0;
			}
			return true;
		}
		return true;
	}

	return TouchState::touchUp(x, y, touchId);
}

void EditorGameState::draw()
{
	mBoard.draw();

	if (mSelected) {
		mSelected->drawNext(&mBoard);
	}

	drawBank();

	gui->draw();
	shared_ptr<ofTrueTypeFont> font = Gui::getFont(1).lock();
	if (font) {
		font->drawString("Frame: " + ofToString(mBoard.getFrame()), 100, 10);
	}
}

void EditorGameState::drawBank()
{
	// bank background
	ofSetColor(Bank::COLOR_BG);
	ofFill();
	ofRect(mBankBounds);

	// draw bank
	for (int i = 0; i < mBankPieces.size(); ++i) {
		if (mBankPieces[i]) {
			ofPushMatrix();
			ofVec2f pos = Board::b2s(i + 0.5f, mBoard.getHeight() + 0.5f);
			ofTranslate(pos);
			mBankPieces[i]->drawPiece(255);
			ofPopMatrix();
		}
	}
}


