#include "Screens.h"

int mListIndex;
ofDirectory mDir;

MainMenuState::MainMenuState()
{
	gui = make_shared<GuiCanvas>(0, 0, ofGetScreenWidth(), ofGetScreenHeight());
	gui->mCanvasBg = ofColor::blueViolet;
	gui->addWidgetDown(shared_ptr<GuiLabel>(new GuiLabel(0, 0, "Singleplayer Games", 1)));
	gui->addWidgetDown(shared_ptr<GuiButton>(new GuiButton(0, 0, 100, 50, "Single Game", this, &MainMenuState::onSingleGame)));
	gui->addWidgetDown(shared_ptr<GuiButton>(new GuiButton(0, 0, 100, 50, "Edit Game", this, &MainMenuState::onEditGame)));
	gui->addWidgetDown(shared_ptr<GuiButton>(new GuiButton(0, 0, 100, 50, "Create Game", this, &MainMenuState::onCreateLevel)));
	gui->addWidgetDown(shared_ptr<GuiLabel>(new GuiLabel(0, 0, "Multiplayer Games", 1)));
	gui->addWidgetDown(shared_ptr<GuiButton>(new GuiButton(0, 0, 100, 50, "Client Game", &RealManager::instance, &RealManager::onClientGame)));
	gui->addWidgetDown(shared_ptr<GuiButton>(new GuiButton(0, 0, 100, 50, "Server Game", &RealManager::instance, &RealManager::onServerGame)));
	gui->addWidgetDown(shared_ptr<GuiButton>(new GuiButton(0, 0, 100, 50, "Toggle Server", &RealManager::instance, &RealManager::toggleServer)));
	gui->addWidgetDown(mBtnServer = shared_ptr<GuiLabel>(new GuiLabel(0, 0, "Server Off")));
	gui->addWidgetDown(shared_ptr<GuiLabel>(new GuiLabel(0, 0, "Misc Games", 1)));
	gui->addWidgetDown(shared_ptr<GuiButton>(new GuiButton(0, 0, 100, 50, "Local Game", &RealManager::instance, &RealManager::onLocalGame)));
	gui->addWidgetDown(shared_ptr<GuiButton>(new GuiButton(0, 0, 100, 50, "Vanilla Game", &RealManager::instance, &RealManager::onVanillaGame)));
	gui->addWidgetDown(shared_ptr<GuiLabel>(new GuiLabel(0, 0, "Player Settings", 1)));
	gui->addWidgetDown(shared_ptr<GuiLabel>(new GuiLabel(0, 0, Game::mPlayerInfo.mClientName)));
	gui->addWidgetDown(shared_ptr<GuiLabel>(new GuiLabel(0, 0, Game::mPlayerInfo.mIp)));
	gui->addWidgetDown(shared_ptr<GuiLabel>(new GuiLabel(0, 0, ofToString(Game::mPlayerInfo.mPort))));
	gui->addWidgetDown(shared_ptr<GuiLabel>(new GuiLabel(0, 0, Game::mPlayerInfo.mLevelFile)));

	vector<string> mLevels;
	mDir.allowExt("xml");
	int cnt = mDir.listDir("levels/");
	for (int i = 0; i < cnt; ++i) {
		mLevels.push_back(mDir.getName(i));
	}
	gui->addWidget(shared_ptr<GuiScrollableListbox>(new GuiScrollableListbox(300, 10, 200, 200, mListIndex, mLevels, 0)));
}

void MainMenuState::enter()
{
	addChild(gui.get());
}

void MainMenuState::exit()
{
	removeChild(gui.get());
}

void MainMenuState::draw()
{
	mBtnServer->setText(RealManager::instance.isServerOn() ? "Server On" : "Server Off");
	gui->draw(); 
}

void MainMenuState::pause()
{
	removeChild(gui.get());
}

void MainMenuState::resume()
{
	addChild(gui.get());
}

void MainMenuState::onEditGame()
{
	Game::mPlayerInfo.mLevelFile = "data/" + mDir.getPath(mListIndex);
	RealManager::instance.onEditGame();
}

void MainMenuState::onSingleGame()
{
	Game::mPlayerInfo.mLevelFile = "data/" + mDir.getPath(mListIndex);
	RealManager::instance.onSingleGame();
}

void MainMenuState::onCreateLevel()
{
	ofDirectory emptyRec("blankLevel.xml");
	string nme = "levels/newLevel" + ofToString(mDir.size()) + ".xml";
	emptyRec.copyTo(nme);
	Game::mPlayerInfo.mLevelFile = "data/" + nme;
	RealManager::instance.onEditGame();
}

