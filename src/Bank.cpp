#include "Bank.h"

using namespace chess;

Bank::Bank()
{
}

Bank::~Bank()
{
	delete mText;
}

void Bank::setup(int color, Board* board)
{
	mBoard = board;
	mId = -1;
	bDragging = false;
	mSelected = 0;
	mBought = 0;

	mPieces.resize(mBoard->getWidth());
	mBounds.set(Board::b2s(0, mBoard->getHeight()), Board::b2s(mBoard->getWidth(), mBoard->getHeight() + 1));
	mText = new TextDraw("Currency: ", Gui::getFont(0), mBounds.x + 10, mBounds.y + mBounds.height - 15, false);
	mColor = color;
}

bool Bank::touchDown(float x, float y, int id)
{
	if (mBounds.inside(x, y)) {
		if (mId == -1) {
			ofVec2f pos = Board::s2b(ofVec2f(x, y));
			Piece* piece = mPieces[(int)(pos.x + 0.001)];

			if(mSelected) {
				if (piece == mSelected) {
					mSelected->bSelected = false;
					mSelected = 0;
				}
			}
			else {
				if (piece) {
					if (piece->getValue() < getCurrency()) {
						mSelected = piece;
						mSelected->bSelected = true;
						mCursor.set(pos);
					}
				}
			}
			mId = id;
		}
		// get piece
		return true;
	}
	return false;
}

bool Bank::touchMoved(float x, float y, int id)
{
	if (mId == id) {
		if (mId != -1) {

			if (mSelected) {
				mCursor = Board::s2b(ofVec2f(x,y));
				bDragging = true;
			}

			return true;
		}
	}
	return false;
}

bool Bank::touchUp(float x, float y, int id)
{
	bDragging = false;

	if (mId == id || mSelected) {
		mId = -1;
		if (mSelected) {
			Position p = Board::s2b(x, y);
			Piece* piece = mBoard->at(p);
			if (mBoard->inBounds(p)) {
				if (piece == 0 && findIn(mBoard->mKings[mColor]->mMoves, p)) {
					AddList.push_back(AddPiece(mSelected->getAscii(), mColor, p));
					mBought -= mSelected->getValue();
				}
				mSelected->bSelected = false;
				mSelected = 0;
			}
		}
		return true;
	}
	return false;
}

void Bank::update()
{
	mText->setText("Currency: " + ofToString(getCurrency()));
	if (mSelected) {
		mBoard->lightUpValid(mBoard->mKings[mColor]);
	}
}

void Bank::draw()
{
	ofSetColor(ofColor::green);
	ofFill();
	ofRect(mBounds);

	ofSetColor(ofColor::navajoWhite);
	mText->draw();

	for (int i = 0; i < mPieces.size(); ++i) {
		if (mPieces[i]) {
			ofPushMatrix();
			ofVec2f pos = Board::b2s(i + 0.5f, mBoard->getHeight() + 0.5f);
			ofTranslate(pos);
			if (mPieces[i]->getValue() < getCurrency())
				mPieces[i]->drawPiece(255);
			else
				mPieces[i]->drawPiece(120);
			ofPopMatrix();
		}
	}

	if (mSelected && bDragging) {
		ofPushMatrix();
		ofTranslate(Board::b2s(mCursor));
		mSelected->drawPiece(50);
		ofPopMatrix();
	}
}

void Bank::setup(const string& bank, int startCash)
{
	for (int i = 0; i < bank.size(); ++i) {
		Piece* pie = mBoard->spawn(bank[i], mColor); // @todo don't use spawn
		mPieces[i++] = pie;
	}
	mBought = startCash;
}

bool Bank::getAdd(AddPiece& ap)
{
	if(!AddList.empty()) {
		ap = AddList.front();
		AddList.pop_front();
		return true;
	}
	else {
		return false;
	}
}

int Bank::getCurrency()
{
	return mBoard->getScore(mColor) + mBought;
}
