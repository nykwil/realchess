#pragma once

#include "Gui.h"
#include "RealChess.h"

class MainMenuState : public TouchState
{
public:
	MainMenuState();
	shared_ptr<GuiCanvas> gui;
	shared_ptr<GuiLabel> mBtnServer;

	void onEditGame();
	void onSingleGame();
	void onCreateLevel();

	virtual void enter();
	virtual void exit();
	virtual void pause();
	virtual void resume();

	virtual void draw();
};

