#include "MultiplayerGame.h"
#include "TouchPlayer.h"
#include "Gui.h"
#include "CmdInfo.h"

using namespace chess;

void ClientGameState::enter()
{
	mBoard.calc();
	mBank.setup(Game::mPlayerInfo.mBank, Game::mPlayerInfo.mStartingCurrency);

	addChild(&mPlayer);
	addChild(&mBank);
}

void ClientGameState::exit()
{
	removeChild(&mPlayer);
	removeChild(&mBank);
}

void ClientGameState::setup(int color, shared_ptr<ClientManager> tcp)
{
	mTcp = tcp;
	assert(mTcp->isConnected());
	mBoard.setup(2, 8, 8, "");
	mBank.setup(color, &mBoard);
	mPlayer.setup(color, &mBoard);
}

void ClientGameState::update(float dt)
{
	if (bConnected) {
		if (mTcp->isConnected()) {
			mPlayer.update();
			mBank.update();

			Maneuver move;
			while (mPlayer.getMove(move)) {
				MoveMsg msg(mPlayer.mColor, mBoard.getFrame(), move);
				if (!mTcp->sendRawBytes((const char*)&msg, MAX_MSG)) {
					ofLogError() << "Can't send message" << endl;
					break;
				}
			}

			AddPiece ad;
			while (mBank.getAdd(ad)) {
				AddPieceMsg msg(mPlayer.mColor, mBoard.getFrame(), ad.mPieceName, ad.mPos);
				if (!mTcp->sendRawBytes((const char*)&msg, MAX_MSG)) {
					ofLogError() << "Can't send message" << endl;
					break;
				}
			}

			// get messages
			int rcv = mTcp->receiveRawBytes(buffer, TCP_MAX_MSG_SIZE);
			for (int i = 0; i < rcv; i += MAX_MSG) {
				if (Game::isDebugMode()) cout << "Client In: " << buffer[i] << endl;

				if (buffer[i] == MSG_ADDPIECE) {
					AddPieceMsg& msg = (AddPieceMsg&)buffer[i];
					mBoard.execute(make_shared<AddCmdInfo>(msg.mTimeStamp, msg.getPiece(), msg.mColor, msg.getPosition()));
				}
				else if (buffer[i] == MSG_MOVE) {
					MoveMsg& msg = (MoveMsg&)buffer[i];
					mBoard.execute(make_shared<StartSlideCmdInfo>(msg.mTimeStamp, msg.getMove()));
				}
				else if (buffer[i] == MSG_ENDGAME) {
					EndGameMsg& msg = (EndGameMsg&)buffer[i];
					if (msg.mLooser == mPlayer.mColor)
						cout << "You Loosed" << endl;
					else
						cout << "You Wonned" << endl;

					RealManager::instance.onQuitGame();
				}
			}

			// @todo should be timed somehow
			mBoard.advance();
		}
		else {
			bConnected = false;
		}
	}
	else {
		// disconnect
		cout << "TODO disconnected go back " << endl;
	}
}

void ClientGameState::draw()
{
	mBoard.draw();
	mPlayer.draw();
	mBank.draw();
}

//////////////////////////////////////////////////////////////////////////

ClientPreState::ClientPreState()
{
	gui = make_shared<GuiCanvas>(0, 0, ofGetWidth(), ofGetHeight());
	mClientState = new ClientGameState();
	mTcp = make_shared<ClientManager>();

	mTime = 0.f;
	mLastRetry = 0.f;
	mRetryInterval = 5.f;
	bSignalReady = false;
	bReady = false;
}

ClientPreState::~ClientPreState()
{
	delete mClientState;
}

void ClientPreState::enter()
{
	gui->addWidget(shared_ptr<GuiButton>(new GuiButton(100, 100, 100, 50, "Ready", this, &ClientPreState::onReady)));
	gui->addWidget(shared_ptr<GuiButton>(new GuiButton(350, 0, 100, 40, "Quit", this, &ClientPreState::onQuit)));
	addChild(gui.get());

	bConnected = mTcp->setup(Game::mPlayerInfo.mIp, Game::mPlayerInfo.mPort);
	mTcp->setMessageDelimiter("\n");
}

void ClientPreState::exit()
{
	removeChild(gui.get());
}

void ClientPreState::update(float dt)
{
	mTime += dt;
	// try to connect to server
	if (!bConnected) {
		if (mLastRetry + mRetryInterval < mTime) {
			bConnected = mTcp->setup(Game::mPlayerInfo.mIp, Game::mPlayerInfo.mPort);
			mLastRetry = mTime;
			bSignalReady = false;
			bSignalConnected = true;
		}
	}
	else {
		if (bSignalConnected) {
			ConnectedMsg msg(Game::mPlayerInfo.mClientName.c_str());
			if (mTcp->sendRawBytes((const char*)&msg, MAX_MSG)) {
				bSignalConnected = false;
			}
		}

		int rcv = mTcp->receiveRawBytes(buffer, TCP_MAX_MSG_SIZE);
		for (int i = 0; i < rcv; i += MAX_MSG) {
			if (buffer[i] == MSG_CONNECTED) {
				ConnectedMsg& msg = (ConnectedMsg&)buffer[i];
				int res = ofFind(mClients, string(msg.mName));
				if (res >= mClients.size()) {
					mClients.push_back(msg.mName);
					mReadys.push_back(false);
				}
			}
			else if (buffer[i] == MSG_READY) {
				ReadyMsg& msg = (ReadyMsg&)buffer[i];
				int res = ofFind(mClients, string(msg.mName));
				if (res < mClients.size()) {
					mReadys[res] = msg.bReady;
				}
				if (Game::mPlayerInfo.mClientName.compare(msg.mName) == 0) {
					bReady = msg.bReady; // temp
				}
			}
			else if (buffer[i] == MSG_START) {
				StartGameMsg& msg = (StartGameMsg&)buffer[i];
				mClientState->setup(msg.mColor, mTcp);
				RealManager::instance.pushState(mClientState);
			}
			else if (buffer[i] == MSG_ADDPIECE) {
				AddPieceMsg& msg = (AddPieceMsg&)buffer[i];
				mClientState->mBoard.cmdAdd(msg.getPiece(), msg.mColor, msg.getPosition());
			}
		}
	}
}

void ClientPreState::draw()
{
	for (int i = 0; i < mClients.size(); ++i) {
		ofFill();
		shared_ptr<ofTrueTypeFont> font = Gui::getFont(1).lock();
		if (font) {
			font->drawString(mClients[i] + " " + ofToString(mReadys[i]), 10, i * 30 + 30);
		}
	}
	gui->draw();
}

void ClientPreState::onReady()
{
	ReadyMsg msg(Game::mPlayerInfo.mClientName.c_str(), !bReady);
	if (!mTcp->sendRawBytes((const char*)&msg, MAX_MSG)) {
		cout << "Can't send message" << endl;
	}
}

void ClientPreState::onQuit()
{
	RealManager::instance.onQuitGame();
}

//////////////////////////////////////////////////////////////////////////

void ServerPreState::enter()
{
	mServer = new ServerGame(); //@TODO delete this
	mServer->setup(Game::mPlayerInfo.mPort);
	mServer->startThread(false, true);
	ClientPreState::enter();
}

void ServerPreState::exit()
{
	ClientPreState::exit();
	mServer->waitForThread(true);
	delete mServer;
}

void ClientManager::sendMsg(BaseMsg& msg)
{

}

static int mIndex = 0;
static int mRcv;

bool ClientManager::receiveMsg()
{
	if (mIndex < mRcv) {
		mIndex += MAX_MSG;
		return true;
	}
	else {
		mRcv = receiveRawBytes(buffer, TCP_MAX_MSG_SIZE);
		mIndex = 0;
		return mRcv > 0;
	}
}

int ClientManager::peekMsgId()
{
	return buffer[mIndex];
}

char* ClientManager::peekMsg()
{
	return &buffer[mIndex];
}
