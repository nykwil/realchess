#pragma once

#include "TouchPlayer.h"
#include "Bank.h"
#include "Piece.h"
#include "RealChess.h"

namespace chess {

// This needs a better name
class AIPieceXml : public PropertyObject {
public:
	AIPieceXml() {
		newProperty("PieceName", mPieceName);
		newProperty("SpawnFrame", mSpawnFrame);
		newProperty("StartPos", mStartPos);
		newProperty("CooldownMult", mCooldownMult, 1.f);
		newProperty("Offsets", mOffsets);
		newPropertyObject("Behavior", mBee);
		mPiece = 0;
	}
	virtual string getType() { return "AIPieceXml"; }

	virtual void setup(Board* board, int color);
	virtual void destroy(Board* board);

	void drawNext(Board* board);
	void editPos(const Position& pos);

	string mPieceName;
	int mSpawnFrame;
	float mCooldownMult;
	Position mStartPos;
	PosList mOffsets;

	shared_ptr<AddOverCmdInfo> addCmd;
	Piece* mPiece;
	SeekerBee* mBee;
};

struct WaveXml : public PropertyObject {
	virtual string getType() { return "Runner"; }
	WaveXml() {
		newContainer("Behaviors", mBehaviors);
	}
	virtual ~WaveXml() {
		for_each(mBehaviors.begin(), mBehaviors.end(), DeleteFromContainer());
	}

	void setup(Board* board, int color);

	vector<AIPieceXml*> mBehaviors; // part of runner info
};

struct GameXml : public PropertyObject {
	virtual string getType() { return "Game"; }
	GameXml() {
		newContainer("Waves", mWaves);
		newProperty("KingStart", mKingStart, Position(5, 0));
		newProperty("SpawnMoneyFrame", mSpawnMoneyFrame, 0);
		newProperty("Layout", mLayout, string(""));
	}
	virtual ~GameXml() {
		for_each(mWaves.begin(), mWaves.end(), DeleteFromContainer());
	}

	string mLayout;
	vector<WaveXml*> mWaves;
	Position mKingStart;
	int mSpawnMoneyFrame;
};

class EditorGameState : public TouchState {

public:
	EditorGameState();

	void load();
	void save();

	void onSave() { save(); }
	void onQuit();
	void onEditBoard();
	void onNextFrame() {
		setFrame(mBoard.getFrame() + 1);
	}
	void onPrevFrame() {
		setFrame(mBoard.getFrame() - 1);
	}
	void onNextStep() {
		setFrame(mBoard.getFrame() + 10);
	}
	void onPrevStep() {
		setFrame(mBoard.getFrame() - 10);
	}

	virtual void enter();
	virtual void exit();

	virtual void draw();

	void drawBank();

	void setFrame(int frame);

	virtual bool touchDown(float x, float y, int touchId);
	virtual bool touchMoved(float x, float y, int touchId);
	virtual bool touchUp(float x, float y, int touchId);

	AIPieceXml* createPiece(Position p);

	virtual void update(float dt);

	shared_ptr<GuiCanvas> gui;
	GameXml mData;
	AIPieceXml* mSelected;
	Piece* mBankSelected;
	vector<Piece*> mBankPieces;
	WaveXml* mCurrWave;

	ofRectangle mBankBounds;
	Board mBoard;

	bool bEditBoard;
};

class SingeplayerGameState : public TouchState {

public:
	SingeplayerGameState();

	virtual void draw();

	virtual void enter();
	virtual void exit();

	virtual void pause() {}
	virtual void resume() {}

	virtual void update(float dt);

	void onQuit();

	Bank mBank;
	TouchPlayer mPlayer;
	Board mBoard;
	int mMoney;

	GameXml mData;
	shared_ptr<GuiLabel> mText;
	shared_ptr<GuiCanvas> gui;
};

};