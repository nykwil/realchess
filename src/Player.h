#pragma once

#include "Board.h"
#include "Touchable.h"

namespace chess
{
	class Board;
	class Maneuver;

	static unsigned int ZERO;

	class Player : public Touchable {
	public:
		virtual void setup(int color, Board* board)
		{
			mColor = color;
			mBoard = board;
		}
		virtual bool getMove(Maneuver& move) = 0;
		virtual void draw() {};
		virtual void update() {};

		int mColor;

		Board* mBoard;
	};

	class HumanPlayer : public Player {
	public:
		bool getMove(Maneuver& move);
		char* readInput(void);
		bool processInput(char* buf, Maneuver& move);
	};

	class AIPlayer: public Player {

	public:
		AIPlayer();
		bool getMove(Maneuver& move);

	protected:
		int evalAlphaBeta(int color, int depth, int alpha, int beta, bool quiescent);
		int mDepth;
	};
	/*
	class AIThread : public ofThread, public Player {

		bool bRunning;

		void startBoard(const Board& board)
		{

		}

		void start() {
			while (!mProcessMoves.empty()) {
				_board.makeMove(mProcessMoves.front());
				mProcessMoves.pop();
			}
			startThread();
		}

		void run() {
			// sync moves
			// _getMove()
			// push move
		}

		void threadedFunc() {
			while (bRunning) {
				run();
			}
		}

		// This gets called on the main thread (where the board belongs to)
		bool getMove(Move& move) {
			if (!mMakeMoves.empty()) {
				move = mMakeMoves.back();
				mMakeMoves.pop();
			}
			// compare the number of moves in both boards and make the last few moves
			// put the last x moves into the mProcessMoves
			for (int i = mNumberOfMoves; i < mBoard->mUndoMoves.size(); ++i) {
				mProcessMoves.push(mBoard->mUndoMoves[i].mMove);
			}
			// mNumberOfMoves = mBoard->mUndoMoves.size();
		}

		bool _getMove(Move& move) {};

		void addMove(const Move& move) {
			mProcessMoves.push(move);
		}

		std::queue<Move> mProcessMoves;
		std::queue<Move> mMakeMoves;

		int mNumberOfMoves;

		Board _board;
	};
	*/
}

