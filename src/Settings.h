#pragma once
#include "RealChessCommon.h"
#include "Gui.h"

namespace chess {

class GameplaySettings : public PropertyObject
{
public:
	GameplaySettings();
	virtual string getType() { return "GameplaySettings"; }
};

class UISettings : public PropertyObject
{
public:
	UISettings();
	virtual string getType() { return "UISettings"; }
};

class Settings
{
public:
	Settings();
	GameplaySettings mGameplay;
	UISettings mUI;

	void onSave();
	void onLoad();


	GuiCanvas gui;
};

};