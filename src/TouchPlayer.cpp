#include "TouchPlayer.h"

namespace chess {

TouchPlayer::TouchPlayer()
{
	mTouchId = -1;
	mSelected = 0;
	mCursor.set(0,0);
	bDragging = false;
	mCharge = 0;
	mTokens = 0;
}

void TouchPlayer::update()
{
	if (mTokens < gMaxTokens) {
		mCharge += 1.f;
		if (mCharge > gMaxCharge) {
			++mTokens;
			mCharge = 0;
		}
	}
	if (mSelected) {
		if (mSelected->bDead) {
			mSelected->bSelected = false;
			mSelected = 0;
		}
		else {
			if (!mSelected->isInCooldown(mBoard->getFrame()))
				mBoard->lightUpValid(mSelected);
		}
	}
}

bool TouchPlayer::getMove(Maneuver& move)
{
	if(!mQueue.empty()) {
		move = mQueue.front();
		mQueue.pop_front();
		return true;
	}
	else {
		return false;
	}
}

void TouchPlayer::draw()
{
	ofFill();
	ofSetColor(ofColor::aliceBlue);
	ofRect(gChargeRect);

	ofSetColor(ofColor::greenYellow);
	ofRect(gChargeRect.x, gChargeRect.y, gChargeRect.width * mCharge / gMaxCharge, gChargeRect.height);

	for (int i = 0; i < mTokens; ++i) {
		ofSetColor(ofColor::aliceBlue);
		ofCircle(gChargeRect.x + gChargeRect.width + gChargeRect.height + (i * gChargeRect.height), gChargeRect.y + gChargeRect.height / 2, gChargeRect.height / 2);
	}

	if (mSelected) {
		if (bDragging) {
			ofPushMatrix();
			ofTranslate(Board::b2s(mCursor));
			mSelected->drawPiece(50);
			ofPopMatrix();
		}
	}
}

bool TouchPlayer::touchDown(float x, float y, int id)
{
	if (mTouchId != -1)
		return false;

	mTouchId = id;
	ofVec2f point(x, y);
	if (mBoard->inside(point)) {
		ofVec2f pos = Board::s2b(point);
		Piece* piece = mBoard->at(Position(pos.x, pos.y));
		if (mSelected) {
			if (piece == mSelected) {
				mSelected->bSelected = false;
				mSelected = 0;
			}
			return true;
		}
		else {
			if (piece && piece->mColor == mColor) {
				mSelected = piece;
				mSelected->bSelected = true;
				mCursor.set(pos);
				return true;
			}
			else {
				return false;
			}
		}
	}
	return false;
}

bool TouchPlayer::touchMoved(float x, float y, int id)
{
	if (mTouchId != id)
		return false;

	if (mSelected) {
		mCursor = Board::s2b(ofVec2f(x,y));
		bDragging = true;
		return true;
	}

	return false;
}

bool TouchPlayer::touchUp( float x, float y, int id )
{
	if (mTouchId != id)
		return false;

	mTouchId = -1;
	bDragging = false;

	if (mSelected) {
		ofVec2f pos = Board::s2b(ofVec2f(x, y));
		Position p;
		if (gSnap) {
			p = mSelected->getNearestPos(pos);
		}
		else {
			p = Position((int)pos.x, (int)pos.y);
		}
		Piece* piece = mBoard->at(p);
		if (mSelected != piece && mTokens > 0 && !mSelected->isInCooldown(mBoard->getFrame())) {
			Maneuver mv(mSelected->mPos, p);
			if (mBoard->isValidMove(mColor, mv)) {
				mQueue.push_back(mv);
				mSelected->bSelected = false;
				mSelected = 0;
			}
			else {
				cout << "TouchPlayer: Invalid Move" << endl;
			}
		}
		return true;
	}
	return false;
}

}; // namespace chess
