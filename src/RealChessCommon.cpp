#include "RealChessCommon.h"
#include "Timer.h"
#include "Loader.h"

int Timer::mDesiredFrameTime;
PlayerXml Game::mPlayerInfo;

void StringConverter::fromString(chess::Position& val, const string& str) {
	val.set(str);
}

string StringConverter::toString(const chess::Position& v) {
	return v.getId();
}

void StringConverter::fromString(chess::PosList& vec, const string& str) {
	StringVector sv = StringConverter::parseStringVector(str);
	vec.resize(sv.size());

	for (int i = 0; i < vec.size(); ++i) {
		StringConverter::fromString(vec[i], sv[i]);
	}
}

string StringConverter::toString(const chess::PosList& vec) {
	if (vec.empty())
		return "";

	string str = toString(vec[0]);
	for (int i = 1; i < vec.size(); ++i) {
		str += ",";
		str += toString(vec[i]);
	}
	return str;
}

bool Game::isDebugMode()
{
	return true;
}

void Game::loadPlayer(const string& filename)
{
	Loader::load(filename, &Game::mPlayerInfo);
}
