#pragma once

#include "Player.h"

namespace chess
{
	const int TOUCH_POINTS = 10;

	class TouchPlayer : public Player {

	public:
		TouchPlayer();

		virtual void update();
		virtual bool getMove(Maneuver& move);
		virtual void draw();
		virtual bool touchMoved(float x, float y, int id);
		virtual bool touchUp(float x, float y, int id);
		virtual bool touchDown(float x, float y, int id);

		static bool gSnap;
		static float gMaxCharge;
		static int gMaxTokens;
		static ofRectangle gChargeRect;

		int mTokens;

	protected:
		list<Maneuver> mQueue;
		bool bDragging;

		float mCharge;

		Piece* mSelected;
		ofVec2f mCursor;
		int mTouchId;
	};
}

