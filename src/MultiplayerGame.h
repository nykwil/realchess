#pragma once

#include "ServerGame.h"
#include "Bank.h"
#include "TouchPlayer.h"

class GuiCanvas;

namespace chess
{
class ClientManager : public ofxTCPClient
{
public:
	char buffer[TCP_MAX_MSG_SIZE];
	void sendMsg(BaseMsg& msg);
	bool receiveMsg();
	int peekMsgId();
	char* peekMsg();
};

class ClientGameState : public TouchState
{
public:
	char buffer[TCP_MAX_MSG_SIZE];
	shared_ptr<ClientManager> mTcp;
	bool bConnected;

	void setup(int color, shared_ptr<ClientManager> tcp);
	virtual void draw();

	virtual void enter();
	virtual void exit();

	Board mBoard;

	TouchPlayer mPlayer;
	Bank mBank;

	virtual void update(float dt);
};

class ClientPreState : public TouchState
{
public:
	char buffer[TCP_MAX_MSG_SIZE];
	shared_ptr<ClientManager> mTcp;
	bool bConnected;
	bool bSignalReady;
	bool bSignalConnected;

	float mLastRetry;
	float mRetryInterval;
	float mTime;

	vector<string> mClients;
	vector<bool> mReadys;

	bool bReady;

	void onReady();
	void onQuit();

	virtual void enter();
	virtual void exit();
	virtual void draw();

	ClientPreState();
	virtual ~ClientPreState();

	void startGame();

	shared_ptr<GuiCanvas> gui;

	virtual void update(float dt);
	ClientGameState* mClientState;
};

class ServerPreState : public ClientPreState
{
public:
	virtual void enter();
	virtual void exit();
	ServerGame* mServer;
};

};