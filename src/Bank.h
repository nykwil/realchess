#pragma once

#include "RealChess.h"
#include "RealChessCommon.h"
#include "Gui.h"

namespace chess {

struct AddPiece
{
	AddPiece() {};
	AddPiece(char pieceName, int color, const Position& pos) : 
		mPieceName(pieceName), mColor(color), mPos(pos) {};

	char mPieceName;
	int mColor;
	Position mPos;
};

class Bank : public Touchable {
public:
	Bank();
	virtual ~Bank();
	void setup(int color, Board* board);

	void update();
	void draw();
	void setup(const string& bank, int startCash);
	void reset() { mSelected = 0; mId = 0; }
	ofRectangle mBounds;

	bool getAdd(AddPiece& ap);

	virtual bool touchMoved(float x, float y, int id);
	virtual bool touchUp(float x, float y, int id);
	virtual bool touchDown(float x, float y, int id);

	int getCurrency();

	list<AddPiece> AddList;
	int mId;
	Piece* mSelected;
	Board* mBoard;
	bool bDragging;
	ofVec2f mCursor;
	vector<Piece*> mPieces;
	int mColor;
	TextDraw* mText;
	int mBought;

	static ofColor COLOR_BG;
};

};